import GUI.GraphTeach;
import backend.Common;

import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class Main {
	public static void main(String[] args) throws Exception {
		UIManager.setLookAndFeel(new NimbusLookAndFeel()); // Set look and feel of the application
		// Process command line arguments
		for (String arg: args) {
			// --remove-prefs removes any persistent settings stored on this machine, implies --no-prefs
			if (arg.matches("(?i)--remove-pref(erences|s)")) {
				try {
					if (Preferences.userRoot().nodeExists("GraphTeach")) {
						Preferences.userRoot().node("GraphTeach").removeNode();
						Preferences.userRoot().flush();
					}
				} catch (BackingStoreException ignored) {}
				Common.noPrefs = true;
				continue;
			}

			// --no-prefs does not save application setting in persistent storage
			if (arg.matches("(?i)--no-pref(erences|s)")) {
				Common.noPrefs = true;
				continue;
			}

			// --help displays command line help
			if (!arg.matches(("(?i)[/-][h?]|--help"))) System.out.println("Unrecognized parameter: " + arg + "\n");
			System.out.println("GraphTeach --help");
			System.out.println("GraphTeach [--no-prefs] [--remove-prefs]");
			System.out.println();
			System.out.println("\t--no-prefs --no-preferences\n\t\tDoesn't persistently store options on the systems\n");
			System.out.println("\t--remove-prefs --remove-preferences\n\t\tRemoves stored preferences, implies --no-prefs\n");
			System.out.println("\t-? -h --help\n\t\t Display this help\n");
			System.exit(1);
		}
		GraphTeach app = GraphTeach.getApp(); // Initialize the application
		Common.init(); // Initialize dot
	}
}
