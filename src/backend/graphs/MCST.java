package backend.graphs;

import com.google.common.graph.ElementOrder;
import com.google.common.graph.ValueGraphBuilder;

/** Class representing a minimum cost spanning tree */
public class MCST extends Graph {

	/** Construct an empty minimum cost spanning tree */
	public MCST() {
		super(ValueGraphBuilder.undirected().allowsSelfLoops(false).nodeOrder(ElementOrder.<Character>natural()).build());
	}

	/** Construct an empty minimum cost spanning tree with an expected node count of {@code size} */
	public MCST(int size) {
		super(ValueGraphBuilder.undirected().allowsSelfLoops(false).nodeOrder(ElementOrder.<Character>natural()).expectedNodeCount(size).build());
	}

	public MCST clone() {
		return (MCST) super.clone();
	}
}
