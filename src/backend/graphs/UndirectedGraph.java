package backend.graphs;

import com.google.common.graph.ElementOrder;
import com.google.common.graph.ValueGraphBuilder;

/** An undirected weighted graph */
public class UndirectedGraph extends Graph {

	/** Constructs an empty undirected weighted graph */
	public UndirectedGraph() {
		super(ValueGraphBuilder.undirected().allowsSelfLoops(false).nodeOrder(ElementOrder.<Character>natural()).build());
	}

	@Override
	public UndirectedGraph clone() {
		return (UndirectedGraph) super.clone();
	}
}
