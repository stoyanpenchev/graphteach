package backend.graphs;

import com.google.common.graph.EndpointPair;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Interface for an immutable view of a graph
 * @see Graph
 */
public interface GraphView {
	/**
	 * Returns a new view of the underlying graph. Equivalent to {@code getView(false)}.
	 * @return a view of the underlying graph
	 * @see #getView(boolean)
	 */
	default GraphView getView() {
		return getView(false);
	}

	/**
	 * Returns a new view of the underlying graph
	 * @param transferHighlights set to {@code true} if you want the new view to start with the same highlights as this view
	 * @return a view of the underlying graph
	 * @see Graph#getView(boolean)
	 */
	GraphView getView(boolean transferHighlights);

	/**
	 * @return {@code true} if the graph is directed
	 * @see Graph#isDirected()
	 */
	boolean isDirected();

	/**
	 * @return a set of all nodes in the graph
	 * @see Graph#nodes()
	 */
	Set<Character> nodes();

	/**
	 * @return a map of all edges in the graph and their associated weights
	 * @see Graph#edges()
	 */
	Map<EndpointPair<Character>, Integer> edges();

	/**
	 * @return the value of the edge connecting {@code n} to node {@code n1}, if one is present; otherwise, returns {@link Optional#empty()}
	 * @see GraphView#edgeValue(char, char)
	 */
	Optional<Integer> edgeValue(char n, char n1);

	/** @return a set of all nodes {@code n} for which {@code isHighlighted(n) == true} */
	Set<Character> highlightedNodes();

	/** @return a set of all edges {@code e} for which {@code isHighlighted(e) == true} */
	Set<EndpointPair<Character>> highlightedEdges();

	/** @return {@code true} if {@code node} is highlighted in this view */
	boolean isHighlighted(char node);

	/** @return {@code true} if {@code edge} is highlighted in this view */
	boolean isHighlighted(EndpointPair<Character> edge);

	/** @return {@code true} if the edge with endpoints {@code n} and {@code n1} is highlighted in this view */
	boolean isHighlighted(char n, char n1);

	/** Highlights the specified {@code node} */
	void setHighlight(char node);

	/** Highlights the specified {@code edge} */
	void setHighlight(EndpointPair<Character> edge);

	/** Highlights the edge specified by the {@code n} and {@code n1} endpoints */
	void setHighlight(char n, char n1);

	/** Clear highlighting from specified {@code node} */
	void clearHighlight(char node);

	/** Clear the highlighting from specified {@code edge} */
	void clearHighlight(EndpointPair<Character> edge);

	/** Clear highlighting from edge specified by {@code n} and {@code n1} endpoints */
	void clearHighlight(char n, char n1);

	/** Clear highlighting from all nodes */
	default void clearHighlightedNodes() {
		highlightedNodes().forEach(this::clearHighlight);
	}

	/** Clear highlighting from all edges */
	default void clearHighlightedEdges() {
		highlightedEdges().forEach(this::clearHighlight);
	}

	/** Clear highlighting from all nodes and edges */
	default void clearHighlights() {
		clearHighlightedNodes();
		clearHighlightedEdges();
	}
}
