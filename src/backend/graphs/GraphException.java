package backend.graphs;

/** A runtime exception associated with graphs and their methods */
public class GraphException extends RuntimeException {

	public GraphException() {
		super();
	}

	public GraphException(String message) {
		super(message);
	}
}
