package backend.graphs;

import com.google.common.graph.ElementOrder;
import com.google.common.graph.ValueGraphBuilder;

/** A directed weighted graph */
public class DirectedGraph extends Graph {

	/** Construct an empty directed weighted graph */
	public DirectedGraph() {
		super(ValueGraphBuilder.directed().allowsSelfLoops(false).nodeOrder(ElementOrder.<Character>natural()).build());
	}

	@Override
	public DirectedGraph clone() {
		return (DirectedGraph) super.clone();
	}
}
