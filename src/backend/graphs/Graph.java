package backend.graphs;

import com.google.common.collect.Maps;
import com.google.common.graph.ElementOrder;
import com.google.common.graph.EndpointPair;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/** Abstract base class for (weighted) graphs */
@SuppressWarnings("ConstantConditions")
public abstract class Graph implements Serializable, Cloneable, GraphView {

	/** Internal graph model */
	protected final MutableValueGraph<Character, Integer> graph;

	/** Map for keeping track of highlighted nodes */
	protected Map<Character, Boolean> highlightedNodes;
	/** Map for keeping track of highlighted edges */
	protected Map<EndpointPair<Character>, Boolean> highlightedEdges;

	/**
	 * List of all views registered with this graph
	 * @see GraphView
	 * @see #getView(boolean)
	 */
	private transient List<Graph> registeredViews;

	/**
	 * Initializes the internal graph model
	 * @param graph the graph model to be used (subclasses must create this)
	 */
	protected Graph(MutableValueGraph<Character, Integer> graph) {
		this.graph = graph;
		highlightedNodes = new HashMap<>();
		highlightedEdges = new HashMap<>();
		registeredViews = new ArrayList<>();
	}

	/** @see GraphView */
	public GraphView getView(boolean transferHighlights) {
		Graph g = new Graph(graph) {
			@Override
			public GraphView getView(boolean transferHighlights) {
				GraphView gv = super.getView(false);
				if (transferHighlights) {
					highlightedNodes().forEach(gv::setHighlight);
					highlightedEdges().forEach(gv::setHighlight);
				}
				return gv;
			}

			@Override
			public boolean addNode() throws GraphException {
				throw new UnsupportedOperationException("Views cannot modify underlying graph");
			}

			@Override
			public boolean addNode(char c) {
				throw new UnsupportedOperationException("Views cannot modify underlying graph");
			}

			@Override
			public void addNodes(int n) {
				throw new UnsupportedOperationException("Views cannot modify underlying graph");
			}

			@Override
			public boolean removeNode(char n) {
				throw new UnsupportedOperationException("Views cannot modify underlying graph");
			}

			@Override
			public Integer addEdge(char n, char n1, int weight) {
				throw new UnsupportedOperationException("Views cannot modify underlying graph");
			}

			@Override
			public Integer removeEdge(char n, char n1) {
				throw new UnsupportedOperationException("Views cannot modify underlying graph");
			}
		};
		g.registeredViews = null;
		g.highlightedNodes.putAll(highlightedNodes);
		g.highlightedEdges.putAll(highlightedEdges);
		if (!transferHighlights) g.clearHighlights();
		registeredViews.add(g);
		return g;
	}

	public final boolean isDirected() {
		return graph.isDirected();
	}

	// Node methods

	/**
	 * Adds a node to the graph with the first available label
	 * @return {@code true} if node was added
	 * @throws GraphException If node count exceeds 26
	 */
	public boolean addNode() throws GraphException {
		if (graph.nodes().size() < 26) {
			char node = 'A';
			if (graph.nodes().size() != 0) {
				if (Collections.max(graph.nodes()) - Collections.min(graph.nodes()) + 1 == graph.nodes().size()) {
					node = (char) (Collections.max(graph.nodes()) + 1);
				} else {
					for (char c = 'A'; c < 'Z'; c++) {
						if (!graph.nodes().contains(c)) {
							node = c;
							break;
						}
					}
				}
			}
			return addNode(node);
		} else throw new GraphException("Too many nodes!");
	}

	/**
	 * Adds node {@code c} to the graph
	 * @param c the node to be added
	 * @return {@code true} if node was added
	 * @throws GraphException If node count exceeds 26
	 */
	public boolean addNode(char c) throws GraphException {
		if (graph.nodes().size() < 26) {
			highlightedNodes.put(c, false);
			registeredViews.forEach(gv -> gv.highlightedNodes.put(c, false));
			return graph.addNode(c);
		} else throw new GraphException("Too many nodes!");
	}

	/**
	 * Add a certain number of nodes to the graph labeled A, B, C...
	 * @param n number of nodes to be added
	 */
	public void addNodes(int n) {
		for (int i = 0; i < n; i++) addNode();
	}

	/**
	 * Removes a node from the graph
	 * @param n node to be removed
	 * @return {@code true} if the node was removed
	 */
	public boolean removeNode(char n) {
		highlightedNodes.remove(n);
		registeredViews.forEach(gv -> gv.highlightedNodes.remove(n));
		return graph.removeNode(n);
	}

	public Set<Character> nodes() {
		return Collections.unmodifiableSet(graph.nodes());
	}

	// Edge methods

	/**
	 * <p>Add an edge to the graph connecting node {@code n} to node {@code n1} and associating {@code weight} with it.</p>
	 * <p>If node {@code n} and node {@code n1} are not already present in the graph, this method will silently add them.</p>
	 * @return the value previously associated with the edge connecting {@code n} to node {@code n1}, or {@code null} if there was no such edge
	 * @throws GraphException If the edge is a self-loop (i.e. when {@code n.equals(n1)})
	 */
	public Integer addEdge(char n, char n1, int weight) throws GraphException {
		try {
			if (!graph.nodes().contains(n)) highlightedNodes.put(n, false);
			if (!graph.nodes().contains(n1)) highlightedNodes.put(n1, false);
			highlightedEdges.put(isDirected() ? EndpointPair.ordered(n, n1) : EndpointPair.unordered(n, n1), false);
			registeredViews.forEach(gv -> gv.highlightedEdges.put(isDirected() ? EndpointPair.ordered(n, n1) : EndpointPair.unordered(n, n1), false));
			return graph.putEdgeValue(n, n1, weight);
		} catch (IllegalArgumentException e) {
			throw new GraphException("Self loops not allowed");
		}
	}

	/**
	 * Remove the edge connecting node {@code n} to node {@code n1}, if it is present
	 * @return the value previously associated with the edge connecting {@code n} to node {@code n1}, or {@code null} if there was no such edge
	 */
	public Integer removeEdge(char n, char n1) {
		highlightedEdges.remove(isDirected() ? EndpointPair.ordered(n, n1) : EndpointPair.unordered(n, n1));
		registeredViews.forEach(gv -> gv.highlightedEdges.remove(isDirected() ? EndpointPair.ordered(n, n1) : EndpointPair.unordered(n, n1)));
		return graph.removeEdge(n, n1);
	}

	/** @return {@code true} if there is an edge connecting node {@code n} to node {@code n1} */
	public boolean hasEdgeConnecting(Character n, Character n1) {
		return graph.hasEdgeConnecting(n, n1);
	}

	public Map<EndpointPair<Character>, Integer> edges() {
		return Collections.unmodifiableMap(graph.edges().stream().collect(Collectors.toMap(
				edge -> edge,
				edge -> graph.edgeValue(edge.nodeU(), edge.nodeV()).get(),
				(a, b) -> b,
				() -> new HashMap<>(graph.edges().size()))));
	}

	@Override
	public Optional<Integer> edgeValue(char n, char n1) {
		return graph.edgeValue(n, n1);
	}

	/** @return the value of the edge connecting {@code n} to node {@code n1}, if one is present; otherwise, returns {@code defaultValue} */
	public Integer edgeValueOrDefault(char n, char n1, @Nullable Integer defaultValue) {
		return graph.edgeValueOrDefault(n, n1, defaultValue);
	}

	// Other methods

	/** @return a set of all nodes adjacent to {@code n} */
	public Set<Character> adjacentNodes(Character n) {
		return Collections.unmodifiableSet(graph.adjacentNodes(n));
	}

	/** @return a set of all adjacent nodes whose edges lead to node {@code n} */
	public Set<Character> predecessors(Character n) {
		return graph.predecessors(n);
	}

	/** @return a set of all adjacent nodes connected through node {@code n}'s outgoing edges */
	public Set<Character> successors(Character n) {
		return graph.successors(n);
	}

	/** @return the number of incoming edges for node {@code n} (equivalent to {@code predecessors(n).size()}) */
	public int inCount(Character n) {
		return graph.inDegree(n);
	}

	/** @return the number of outgoing edges for node {@code n} (equivalent to {@code successors(n).size()}) */
	public int outCount(Character n) {
		return graph.outDegree(n);
	}

	/** @return the number of nodes adjacent to {@code n} (equivalent to {@code adjacentNodes(n).size()}) */
	public int neighbourCount(Character n) {
		return graph.degree(n);
	}

	// Highlighting methods

	public Set<Character> highlightedNodes() {
		return highlightedNodes.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).collect(Collectors.toSet());
	}

	public Set<EndpointPair<Character>> highlightedEdges() {
		return highlightedEdges.entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).collect(Collectors.toSet());
	}

	public boolean isHighlighted(char node) {
		return highlightedNodes.containsKey(node) && highlightedNodes.get(node);
	}

	public boolean isHighlighted(EndpointPair<Character> edge) {
		if (isDirected() != edge.isOrdered()) edge = isDirected() ? EndpointPair.ordered(edge.nodeU(), edge.nodeV()) : EndpointPair.unordered(edge.nodeU(), edge.nodeV());
		if (isDirected() && !highlightedEdges.containsKey(edge)) edge = EndpointPair.ordered(edge.nodeV(), edge.nodeU());
		return highlightedEdges.containsKey(edge) && highlightedEdges.get(edge);
	}

	public boolean isHighlighted(char n, char n1) {
		return isHighlighted(EndpointPair.unordered(n, n1));
	}

	public void setHighlight(char node) {
		highlightedNodes.replace(node, true);
	}

	public void setHighlight(EndpointPair<Character> edge) {
		if (isDirected() != edge.isOrdered()) edge = isDirected() ? EndpointPair.ordered(edge.nodeU(), edge.nodeV()) : EndpointPair.unordered(edge.nodeU(), edge.nodeV());
		if (isDirected() && !highlightedEdges.containsKey(edge)) edge = EndpointPair.ordered(edge.nodeV(), edge.nodeU());
		highlightedEdges.replace(edge, true);
	}

	public void setHighlight(char n, char n1) {
		setHighlight(EndpointPair.unordered(n, n1));
	}

	public void clearHighlight(char node) {
		highlightedNodes.replace(node, false);
	}

	public void clearHighlight(EndpointPair<Character> edge) {
		if (isDirected() != edge.isOrdered()) edge = isDirected() ? EndpointPair.ordered(edge.nodeU(), edge.nodeV()) : EndpointPair.unordered(edge.nodeU(), edge.nodeV());
		if (isDirected() && !highlightedEdges.containsKey(edge)) edge = EndpointPair.ordered(edge.nodeV(), edge.nodeU());
		highlightedEdges.replace(edge, false);
	}

	public void clearHighlight(char n, char n1) {
		clearHighlight(EndpointPair.unordered(n, n1));
	}

	// Serialization methods

	protected void writeObject(ObjectOutputStream out) throws IOException {
		out.writeBoolean(graph.isDirected());
		out.writeObject(new HashSet<>(nodes()));
		out.writeObject(graph.edges().stream().collect(Collectors.toMap(e -> e.nodeU().toString() + e.nodeV().toString(), e -> graph.edgeValue(e.nodeU(), e.nodeV()).get())));
		out.writeObject(highlightedNodes);
		out.writeObject(highlightedEdges.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().nodeU().toString() + e.getKey().nodeV().toString(), Map.Entry::getValue)));
	}

	@SuppressWarnings("unchecked")
	protected void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		try {
			Field graphField = Graph.class.getDeclaredField("graph");
			graphField.setAccessible(true);
			graphField.set(this, (in.readBoolean() ? ValueGraphBuilder.directed() : ValueGraphBuilder.undirected()).allowsSelfLoops(false).nodeOrder(ElementOrder.<Character>natural()).build());
		} catch (NoSuchFieldException | IllegalAccessException ignored) {}
		((Set<Character>) in.readObject()).forEach(graph::addNode);
		((Map<String, Integer>) in.readObject()).forEach((edge, weight) -> graph.putEdgeValue(edge.charAt(0), edge.charAt(1), weight));
		highlightedNodes = (Map<Character, Boolean>) in.readObject();
		highlightedEdges = ((Map<String, Boolean>) in.readObject()).entrySet().stream().collect(Collectors.toMap(e -> isDirected() ?
				EndpointPair.ordered(e.getKey().charAt(0), e.getKey().charAt(1)) : EndpointPair.unordered(e.getKey().charAt(0), e.getKey().charAt(1)), Map.Entry::getValue));
		registeredViews = new ArrayList<>();
	}

	// Object methods

	/** @return a copy of the graph */
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	@Override
	public Graph clone() {
		try {
			Graph ret = this.getClass().getConstructor().newInstance();
			graph.nodes().forEach(ret.graph::addNode);
			edges().forEach((edge, weight) -> ret.graph.putEdgeValue(edge.nodeU(), edge.nodeV(), weight));
			ret.highlightedNodes = new HashMap<>(highlightedNodes);
			ret.highlightedEdges = new HashMap<>(highlightedEdges);
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String toString() {
		return "directed: " + graph.isDirected() + ", nodes: " + graph.nodes() + ", edges: " +
				Maps.asMap(graph.edges(), edge -> graph.edgeValueOrDefault(edge.nodeU(), edge.nodeV(), null));
	}
}
