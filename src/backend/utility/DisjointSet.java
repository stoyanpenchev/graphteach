package backend.utility;

import com.google.common.collect.UnmodifiableIterator;

import java.util.*;

/**
 * Class emulating the <a href="https://en.wikipedia.org/wiki/Disjoint-set_data_structure">Disjoint-Set Data Structure</a>
 * @param <T> type of elements in the set
 */
public class DisjointSet<T> implements Iterable<Set<T>> {

	/** A map of all elements to the sets they belong to */
	private Map<T, Set<T>> sets;

	/** Constructs a new empty DisjointSet */
	public DisjointSet() {
		sets = new HashMap<>();
	}

	/** Constructs a new DisjointSet with the elements of {@code elems} */
	public DisjointSet(Collection<T> elems) {
		sets = new HashMap<>(elems.size());
		elems.forEach(this::makeSet);
	}

	/** Constructs a new DisjointSet with elements {@code elems} */
	@SafeVarargs
	public DisjointSet(T... elems) {
		this(Arrays.asList(elems));
	}

	/** Creates a copy of {@code other} */
	public DisjointSet(DisjointSet<T> other) {
		sets = new HashMap<>(other.sets.size());
		other.sets.values().forEach(v -> {
			Set<T> set = new TreeSet<>(v);
			set.forEach(x -> sets.put(x, set));
		});
	}

	/** Adds element {@code x} if it is not already in the set and associates it with its own single-member set */
	public void makeSet(T x) {
		if (!sets.containsKey(x)) {
			Set<T> set = new TreeSet<>();
			set.add(x);
			sets.put(x, set);
		}
	}

	/** @return an unmodifiable view of the set element {@code x} is associated with if there is one, {@code null} otherwise */
	public Set<T> findSet(T x) {
		return sets.containsKey(x) ? Collections.unmodifiableSet(sets.get(x)) : null;
	}

	/**
	 * @return {@code true} if elements {@code x} and {@code y} are in the same set;
	 * {@code false} if either {@code x} or {@code y} is not in a set
	 */
	public boolean inSameSet(T x, T y) {
		return sets.containsKey(x) && sets.containsKey(y) && sets.get(x) == sets.get(y);
	}

	/**
	 * Merges the sets elements {@code x} and {@code y} are part of if they are not already in the same set
	 * @throws IllegalArgumentException if either {@code x} or {@code y} is not in the set
	 */
	public void merge(T x, T y) {
		if (!sets.containsKey(x)) throw new IllegalArgumentException("x not in set");
		else if (!sets.containsKey(y)) throw new IllegalArgumentException("y not in set");
		else if (sets.get(x) != sets.get(y)) {
			sets.get(x).addAll(sets.get(y));
			sets.get(y).forEach(z -> sets.replace(z, sets.get(x)));
		}
	}

	@SuppressWarnings("NullableProblems")
	@Override
	public UnmodifiableIterator<Set<T>> iterator() {
		return new UnmodifiableIterator<>() {
			private Iterator<Set<T>> it = sets.values().stream().distinct().iterator();

			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public Set<T> next() {
				return Collections.unmodifiableSet(it.next());
			}
		};
	}

	@Override
	public String toString() {
		StringJoiner ret = new StringJoiner(" ");
		sets.values().stream().distinct().forEach(s -> {
			StringJoiner set = new StringJoiner(",", "(", ")");
			s.forEach(e -> set.add(e.toString()));
			ret.add(set.toString());
		});
		return ret.toString();
	}
}
