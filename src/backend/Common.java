package backend;

import GUI.GraphTeach;
import backend.graphs.GraphException;
import backend.graphs.GraphView;
import backend.graphs.MCST;
import com.google.common.graph.EndpointPair;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/** Class containing static methods and constants that can be used by all parts of the program */
@SuppressWarnings("SpellCheckingInspection")
public final class Common {
	
	/** Private constructor makes class non-instantiable */
	private Common() {}

	/**
	 * Class for customizing user options
	 * @see #getOptions()
	 * @see #setOptions(Options) setOptions()
	 */
	public static class Options implements Cloneable {

		/** Enumeration of possible node shapes */
		public enum NodeShape {
			CIRCLE, OVAL, SQUARE, DIAMOND, TRIANGLE, PENTAGON, HEXAGON, HEPTAGON, OCTAGON;

			/**
			 * Returns the name of this shape in title case
			 * @return the name of this shape */
			@Override
			public String toString() {
				char[] arr = name().toCharArray();
				StringBuilder ret = new StringBuilder(arr.length);
				ret.append(arr[0]);
				for (int i = 1; i < arr.length; i++) ret.append(Character.toLowerCase(arr[i]));
				return ret.toString();
			}
		}

		/**
		 * The default shape of nodes
		 * @see NodeShape
		 */
		public NodeShape NODE_SHAPE = NodeShape.CIRCLE;
		/**
		 * The default shape of result nodes
		 * @see NodeShape
		 */
		public NodeShape RESULT_NODE_SHAPE = NODE_SHAPE;

		/** The default colour of nodes */
		public Color NODE_COLOR = new Color(0xD3D3D3);
		/** The default colour of result nodes */
		public Color RESULT_NODE_COLOR = NODE_COLOR;

		/** The default colour to be used when highlighting nodes, edges or other components */
		public Color HIGHLIGHT_COLOR = Color.GREEN;
		/** The default colour to be used when highlighting nodes, edges or other components of the result */
		public Color RESULT_HIGHLIGHT_COLOR = HIGHLIGHT_COLOR;

		/**
		 * Equivalent to {@code NODE_SHAPE.equals(RESULT_NODE_SHAPE) && NODE_COLOR.equals(RESULT_NODE_COLOR) && HIGHLIGHT_COLOR.equals(RESULT_HIGHLIGHT_COLOR)}
		 * @return {@code true} if all options are the same for result */
		public boolean sameOptionsForResults() {
			return NODE_SHAPE.equals(RESULT_NODE_SHAPE) && NODE_COLOR.equals(RESULT_NODE_COLOR) && HIGHLIGHT_COLOR.equals(RESULT_HIGHLIGHT_COLOR);
		}

		/** @return {@code true} if {@code obj} is an instance of Options and all options are equal */
		@Override
		public boolean equals(Object obj) {
			return obj instanceof Options &&
					NODE_SHAPE.equals(((Options) obj).NODE_SHAPE) && NODE_COLOR.equals(((Options) obj).NODE_COLOR) && HIGHLIGHT_COLOR.equals(((Options) obj).HIGHLIGHT_COLOR) &&
					RESULT_NODE_SHAPE.equals(((Options) obj).RESULT_NODE_SHAPE) && RESULT_NODE_COLOR.equals(((Options) obj).RESULT_NODE_COLOR) && RESULT_HIGHLIGHT_COLOR.equals(((Options) obj).RESULT_HIGHLIGHT_COLOR);
		}

		/** @return a new instance of Options identical to this one */
		@SuppressWarnings("MethodDoesntCallSuperMethod")
		public Options clone() {
			Options ret = new Options();
			ret.NODE_SHAPE = NODE_SHAPE;
			ret.NODE_COLOR = NODE_COLOR;
			ret.HIGHLIGHT_COLOR = HIGHLIGHT_COLOR;
			ret.RESULT_NODE_SHAPE = RESULT_NODE_SHAPE;
			ret.RESULT_NODE_COLOR = RESULT_NODE_COLOR;
			ret.RESULT_HIGHLIGHT_COLOR = RESULT_HIGHLIGHT_COLOR;
			return ret;
		}

	}

	/**
	 * <p>Controls whether setting are persistently stored on system</p>
	 * <p><i>Note: changing this variable during runtime has no effect as it is only used for initialization</i></p>
	 * @see #PREFS
	 */
	public static boolean noPrefs;

	 /**
	  * The active Options currently used by the application
	  * @see #getOptions()
	  * @see #setOptions(Options) setOptions()
	  */
	private static Options OPTIONS;
	/**
	 * Link to persistent storage
	 * @see #noPrefs
	 */
	private static Preferences PREFS;

	/**
	 * Initializes and/or returns the active Options
	 * @return the active Options instance used by the application
	 * @see #OPTIONS
	 * @see #PREFS
	 * @see #noPrefs
	 */
	public static Options getOptions() {
		if (OPTIONS == null) { // Initialize OPTIONS
			try {
				if (Preferences.userRoot().nodeExists("GraphTeach")) { // If persistent options are found
					PREFS = Preferences.userRoot().node("GraphTeach");
					OPTIONS = new Options();
					OPTIONS.NODE_SHAPE = Options.NodeShape.values()[PREFS.getInt("NODE_SHAPE", OPTIONS.NODE_SHAPE.ordinal())];
					OPTIONS.NODE_COLOR = new Color(PREFS.getInt("NODE_COLOR", OPTIONS.NODE_COLOR.getRGB()), true);
					OPTIONS.HIGHLIGHT_COLOR = new Color(PREFS.getInt("HIGHLIGHT_COLOR", OPTIONS.HIGHLIGHT_COLOR.getRGB()), true);
					OPTIONS.RESULT_NODE_SHAPE = Options.NodeShape.values()[PREFS.getInt("RESULT_NODE_SHAPE", OPTIONS.RESULT_NODE_SHAPE.ordinal())];
					OPTIONS.RESULT_NODE_COLOR = new Color(PREFS.getInt("RESULT_NODE_COLOR", OPTIONS.RESULT_NODE_COLOR.getRGB()), true);
					OPTIONS.RESULT_HIGHLIGHT_COLOR = new Color(PREFS.getInt("RESULT_HIGHLIGHT_COLOR", OPTIONS.RESULT_HIGHLIGHT_COLOR.getRGB()), true);
				} else {
					PREFS = noPrefs ? null : Preferences.userRoot().node("GraphTeach");
					setOptions(new Options());
				}
			} catch (BackingStoreException ex) {
				PREFS = null;
				OPTIONS = new Options();
			}
		}
		return OPTIONS;
	}

	/**
	 * Set the active Options used by the application and update the persistent storage if required
	 * @param options the new Options to be used by the application
	 * @see #OPTIONS
	 * @see #PREFS
	 * @see #noPrefs
	 */
	public static void setOptions(Options options) {
		Common.OPTIONS = options;
		if (PREFS != null) {
			// Store options persistently
			try {
				PREFS.putInt("NODE_SHAPE", OPTIONS.NODE_SHAPE.ordinal());
				PREFS.putInt("NODE_COLOR", OPTIONS.NODE_COLOR.getRGB());
				PREFS.putInt("HIGHLIGHT_COLOR", OPTIONS.HIGHLIGHT_COLOR.getRGB());
				PREFS.putInt("RESULT_NODE_SHAPE", OPTIONS.RESULT_NODE_SHAPE.ordinal());
				PREFS.putInt("RESULT_NODE_COLOR", OPTIONS.RESULT_NODE_COLOR.getRGB());
				PREFS.putInt("RESULT_HIGHLIGHT_COLOR", OPTIONS.HIGHLIGHT_COLOR.getRGB());
				PREFS.sync();
			} catch (BackingStoreException ignored) {}
		}
	}

	/** The path to the dot executable */
	private static File DOTPATH;
	/** Initializes dot, if it is not found the user is asked to provide path to "dot.exe" */
	public static void init() {
		if (DOTPATH == null) {
			try {
				DOTPATH = new File(Common.class.getResource("/Graphviz/bin/dot.exe").toURI()); // Initialization of dot.exe path
			} catch (URISyntaxException | NullPointerException e) {
				// Path field
				JTextField location = new JTextField(35);
				location.setPreferredSize(new Dimension(0, 28));
				location.setMaximumSize(new Dimension(Integer.MAX_VALUE, 28));
				location.setEditable(false);
				location.setBorder(BorderFactory.createCompoundBorder(location.getBorder(), BorderFactory.createEmptyBorder(3,0,3,0)));

				// Settiug up "Browse" button
				JButton browseButton = new JButton("Browse");
				browseButton.addActionListener(e1 ->  {
					JFileChooser chooser = new JFileChooser();
					chooser.setAcceptAllFileFilterUsed(false);
					chooser.setFileFilter(new FileFilter() {
						@Override
						public boolean accept(File f) {
							return f.isDirectory() || f.getName().equalsIgnoreCase("dot.exe");
						}

						@Override
						public String getDescription() {
							return "dot.exe";
						}
					});
					if (chooser.showOpenDialog((JButton) e1.getSource()) == JFileChooser.APPROVE_OPTION) {
						location.setText((DOTPATH = chooser.getSelectedFile()).getAbsolutePath());
					}
				});

				final Consumer<Container> setBackgrounds = comp -> {
					comp.setBackground(Color.WHITE);
					List<Component> comps = new ArrayList<>(Arrays.asList(comp.getComponents()));
					for (int i = 0; i < comps.size(); i++) {
						if (comps.get(i) instanceof Container) {
							if (comps.get(i) instanceof JPanel) comps.get(i).setBackground(Color.WHITE);
							comps.addAll(Arrays.asList(((Container) comps.get(i)).getComponents()));
						}
					}
				};

				// Setting up "OK" button
				JButton OKButton = new JButton("OK");
				OKButton.addActionListener(e1 -> {
					((JButton) e1.getSource()).getTopLevelAncestor().setVisible(false);
					if (location.getText().length() == 0) {
						// Create "Application closing..." message
						JDialog dialog = new JDialog(GraphTeach.getApp(), "Application Closing", true);
						dialog.setContentPane(new JOptionPane("Application will close...", JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[0]));
						setBackgrounds.accept(dialog.getContentPane());
						dialog.setResizable(false);
						dialog.pack();
						dialog.setLocationRelativeTo(dialog.getOwner());
						Timer timer = new Timer(2000, a -> System.exit(2)); // Display for 2 seconds then exit
						timer.setRepeats(false);
						timer.start();
						dialog.setVisible(true);
					}
				});

				// Creating dialog
				JDialog dialog = new JDialog(GraphTeach.getApp(), "File Not found", true);
				dialog.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent e) {
						OKButton.doClick();
					}
				});
				dialog.setContentPane(new JOptionPane("Application 'dot', required for visualization, was not found! Please browse to location:",
						JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[] {location, browseButton, OKButton}, browseButton));
				setBackgrounds.accept(dialog.getContentPane());
				dialog.setResizable(false);
				dialog.pack();
				dialog.setLocationRelativeTo(dialog.getOwner());
				dialog.setVisible(true);
			}
		}
	}

	/**
	 * @return a hex string for colour {@code c} in {@code RRGGBBAA} format
	 * (<b>R</b>ed, <b>G</b>reen, <b>B</b>lue, <b>A</b>lpha)
	 */
	private static String toHex(Color c) {
		return String.format("%02X%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}

	/**
	 * Shorthand for full {@code dot(...)} function
	 * @see #dot(GraphView, boolean, Map, Boolean, Color, Color, Color, Color, Color, Color) dot(GraphView graph, boolean result, ...) */
	public static BufferedImage dot(GraphView graph) {
		return dot(graph, false);
	}

	/**
	 * Shorthand for full {@code dot(...)} function
	 * @see #dot(GraphView, boolean, Map, Boolean, Color, Color, Color, Color, Color, Color) dot(GraphView graph, boolean result, ...) */
	public static BufferedImage dot(MCST mcst) {
		return dot(mcst, true);
	}

	/**
	 * Shorthand for full {@code dot(...)} function
	 * @see #dot(GraphView, boolean, Map, Boolean, Color, Color, Color, Color, Color, Color) dot(GraphView graph, boolean result, ...) */
	public static BufferedImage dot(GraphView graph, boolean result) {
		return dot(graph, result, Map.of());
	}

	/**
	 * Shorthand for full {@code dot(...)} function
	 * @see #dot(GraphView, boolean, Map, Boolean, Color, Color, Color, Color, Color, Color) dot(GraphView graph, boolean result, ...) */
	public static BufferedImage dot(GraphView graph, boolean result, boolean useNeato) {
		return dot(graph, result, Map.of(), useNeato, null, null, null, null, null, null);
	}

	/**
	 * Shorthand for full {@code dot(...)} function
	 * @see #dot(GraphView, boolean, Map, Boolean, Color, Color, Color, Color, Color, Color) dot(GraphView graph, boolean result, ...) */
	public static BufferedImage dot(GraphView graph, boolean result, Map<String, Color> bindings) {
		return dot(graph, result, bindings, null, null, null, null, null, null, null);
	}

	/**
	 * <p>Creates an image of the graph using dot.</p>
	 * <br>
	 * <p>The {@code bindings} parameter is a map of formatted strings to colours used for assigning individual colors to
	 * different components of the graph. The following formatted strings are accepted and they override any default values:</p>
	 *     <ul>
	 * 	       <li><i>n</i> - the outline colour of all nodes</li>
	 * 	       <li><i>f</i> - the fill colour of all nodes</li>
	 * 	       <li><i>v</i> - the font colour of all nodes</li>
	 * 	       <li><i>e</i> - the colour of all edges</li>
	 * 	       <li><i>w</i> - the colour of all weight values</li>
	 * 	       <li><i>h</i> - the highlight colour</li>
	 * 	       <li><i>nX</i> - the outline colour of node X</li>
	 * 	       <li><i>fX</i> - the fill colour of node X</li>
	 * 	       <li><i>vX</i> - the font colour of node X</li>
	 * 	       <li><i>eXY</i> - the colour of edge X -- Y</li>
	 * 	       <li><i>wXY</i> - the weight value colour of edge X -- Y</li>
	 * 	   </ul>
	 * <br>
	 * <p>The 6 Color parameters can be used to override the default values from the options, or set to {@code null} to use the defaults.
	 * The priority from high to low is: bindings → parameter overrides → options.</p>
	 * @param graph the graph to be visualized
	 * @param result {@code true} if {@code graph} should be treated as a result
	 * @param bindings colour bindings map as described above
	 * @param useNeato {@code true} if Neato should be used for the layout of the graph, Dot otherwise
	 * @param node the default outline colour of nodes
	 * @param nodeFill the default fill colour of nodes
	 * @param nodeLabel the default font colour of node labels
	 * @param edge the default colour of edges
	 * @param edgeLabel the default weight value colour of edges
	 * @param highlight the default highlight colour
	 * @return an image of the graph
	 * @throws GraphException if the image could not be created
	 */
	public static BufferedImage dot(GraphView graph, boolean result, Map<String, Color> bindings, Boolean useNeato, Color node, Color nodeFill, Color nodeLabel, Color edge, Color edgeLabel, Color highlight) {
		if (useNeato == null) useNeato = !result;
		if (node == null || bindings.containsKey("n")) node = bindings.getOrDefault("n", Color.BLACK);
		if (nodeFill == null || bindings.containsKey("f")) nodeFill = bindings.getOrDefault("f", result ? getOptions().RESULT_NODE_COLOR : getOptions().NODE_COLOR);
		if (nodeLabel == null || bindings.containsKey("v")) nodeLabel = bindings.getOrDefault("v", Color.BLACK);
		if (edge == null || bindings.containsKey("e")) edge = bindings.getOrDefault("e", Color.BLACK);
		if (edgeLabel == null || bindings.containsKey("w")) edgeLabel = bindings.getOrDefault("w", Color.BLACK);
		if (highlight == null || bindings.containsKey("h")) highlight = bindings.getOrDefault("h", result ? getOptions().RESULT_HIGHLIGHT_COLOR : getOptions().HIGHLIGHT_COLOR);
		try {
			if (!graph.isDirected()) {
				for (String k : new HashSet<>(bindings.keySet())) {
					if ((k.startsWith("e") || k.startsWith("w")) && k.length() > 1) {
						bindings.put(Character.toString(k.charAt(0)) + k.charAt(2) + k.charAt(1), bindings.get(k));
					}
				}
			}
		} catch (UnsupportedOperationException ex) {
			bindings = new HashMap<>(bindings);
			for (String k : bindings.keySet()) {
				if ((k.startsWith("e") || k.startsWith("w")) && k.length() > 1) {
					bindings.put(Character.toString(k.charAt(0)) + k.charAt(2) + k.charAt(1), bindings.get(k));
				}
			}
		}

		try {
			Process dot = Runtime.getRuntime().exec(DOTPATH + " -Tpng -K" + (useNeato ? "neato" : "dot") + " -Nshape=" +
					(result ? getOptions().RESULT_NODE_SHAPE : getOptions().NODE_SHAPE).name().toLowerCase() + " -Nstyle=filled -Ncolor=\"#" +
					toHex(node) + "\" -Nfillcolor=\"#" + toHex(nodeFill) + "\" -Nfontcolor=\"#" + toHex(nodeLabel) +
					"\" -Ecolor=\"#" + toHex(edge) + "\" -Efontcolor=\"#" + toHex(edgeLabel) + "\"");

			try (PrintWriter f = new PrintWriter(dot.getOutputStream())) {
				f.println((graph.isDirected() ? "di" : "") + "graph {");
				if (!graph.isDirected() && !useNeato) f.println("rankdir=BT;");
				for (Character n: graph.nodes()) {
					f.print(n);
					StringJoiner nodeOptions = new StringJoiner(", ", " [", "];");
					nodeOptions.setEmptyValue(";");
					if (bindings.containsKey("n" + n))
						nodeOptions.add("color=\"#" + toHex(bindings.get("n" + n)) + "\"");
					if (graph.isHighlighted(n) || bindings.containsKey("f" + n))
						nodeOptions.add("fillcolor=\"#" + toHex(bindings.getOrDefault("f" + n, highlight)) + "\"");
					if (bindings.containsKey("v" + n))
						nodeOptions.add("fontcolor=\"#" + toHex(bindings.get("v" + n)) + "\"");
					f.println(nodeOptions.toString());
				}
				for (Map.Entry<EndpointPair<Character>, Integer> e: graph.edges().entrySet()) {
					f.print(e.getKey().nodeU() + " -" + (graph.isDirected() ? "> " : "- ") + e.getKey().nodeV());
					StringJoiner edgeOptions = new StringJoiner(", ", " [", "];");
					edgeOptions.add("label=\" " + e.getValue() + " \"");
					if (graph.isHighlighted(e.getKey())) {
						edgeOptions.add("color=\"#" + toHex(highlight) + "\"");
						edgeOptions.add("fontcolor=\"#" + toHex(highlight) + "\"");
					} else {
						if (bindings.containsKey("e" + e.getKey().nodeU() + e.getKey().nodeV()))
							edgeOptions.add("color=\"#" + toHex(bindings.get("e" + e.getKey().nodeU() + e.getKey().nodeV())) + "\"");
						if (bindings.containsKey("w" + e.getKey().nodeU() + e.getKey().nodeV()))
							edgeOptions.add("fontcolor=\"#" + toHex(bindings.get("w" + e.getKey().nodeU() + e.getKey().nodeV())) + "\"");
					}
					f.println(edgeOptions.toString());
				}
				f.print('}');
			}

			return ImageIO.read(dot.getInputStream());
		} catch (Exception ex) {
			throw new GraphException("Could not create image for graph");
		}
	}
}
