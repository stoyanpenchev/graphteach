package backend.algorithms;

import GUI.utility.JPanel;
import backend.Common;
import backend.graphs.DirectedGraph;
import backend.graphs.Graph;
import com.google.common.graph.EndpointPair;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static backend.Common.getOptions;

/** Implements Dijkstra's shortest path algorithm */
@SuppressWarnings("ConstantConditions")
public class DijkstrasShortestPath extends Algorithm {

	/** @see Algorithm#getName() */
	public static final String NAME = "Dijkstra's Algorithm";

	public static Color SOURCE_NODE_COLOR = Color.BLUE;
	public static Color TARGET_NODE_COLOR = Color.RED;

	/** Represents the internal state of Dijkstra's algorithm at a specific step of its execution */
	public class DijkstrasState extends State {

		/**
		 * A set of all edges the algorithm hasn't "seen" at this step of its execution. This is used for visualization
		 * purposes and is not necessary for the algorithm's execution.
		 */
		private Set<EndpointPair<Character>> unseenEdges;
		/**
		 * The highlighted entry in the distances table. This is used for visualization purposes and is not necessary
		 * for the algorithm's execution.
		 */
		private Character highlightedEntry;

		/** A set of all nodes that the algorithm has finished processing at this step of its execution */
		private Set<Character> visited;
		/** A set of all nodes that the algorithm "knows" about and can select from at the next stage */
		private Set<Character> frontier;
		/** A map for each node int the {@link #frontier} set to the shortest distance known to it from the start node */
		private Map<Character, Integer> dist;
		/** A map for each node in the {@link #frontier} set to the previous node on the shortest path known to it from the start node */
		private Map<Character, Character> prev;

		/**
		 * <p>The result at this step of the algorithm's execution.</p><br>
		 * <p>It has a type of {@link Object} because while the
		 * intermediate results are {@link DirectedGraph Directed Graphs}, the final result is a {@link Stack} of nodes
		 * which represents the shortest path from the start node to the end node.</p>
		 */
		private Object res;

		/** Constructor for the initial state of the algorithm */
		DijkstrasState() {
			super("• Initialize the visited set to include " + source + "\n" +
					"• Add all successors of " + source + " to the frontier set and update the table with their respective distances");
			graphView.setHighlight(source); // Highlight start node
			// Initialize visited set and add start node to it
			visited = new HashSet<>(1);
			visited.add(source);
			frontier = new HashSet<>(graph.successors(source)); // Initialize frontier set to include all of the start node's successors
			// Initialize the distance map with nodes from the frontier set
			dist = frontier.stream().collect(Collectors.toMap(v -> v, v -> graph.edgeValue(source, v).get(), (a, b) -> b, () -> new HashMap<>(frontier.size())));
			// Initialize the previous nodes map with nodes from the frontier set
			prev = frontier.stream().collect(Collectors.toMap(v -> v, v -> source, (a, b) -> b, () -> new HashMap<>(frontier.size())));
			// Initialize the intermediate result to include the start node and all it successors
			res = new DirectedGraph();
			((DirectedGraph) res).addNode(source);
			frontier.forEach(v -> ((DirectedGraph) res).addEdge(source, v, graph.edgeValue(source, v).get()));
			((DirectedGraph) res).setHighlight(source);
			// Update the unseen edges set
			unseenEdges = new HashSet<>(graph.edges().keySet());
			unseenEdges.removeIf(e -> (e.nodeU() == source && frontier.contains(e.nodeV())) || (!e.isOrdered() && (e.nodeV() == source && frontier.contains(e.nodeU()))));

		}

		/** Constructor for the intermediate states of the algorithm */
		DijkstrasState(Set<EndpointPair<Character>> unseenEdges, Set<Character> visited, Set<Character> frontier, Map<Character, Integer> dist, Map<Character, Character> prev,
		               DirectedGraph res, String message, Character highlightedEntry) {
			super(message);
			this.unseenEdges = new HashSet<>(unseenEdges);
			this.visited = new HashSet<>(visited);
			this.frontier = new HashSet<>(frontier);
			this.dist = new HashMap<>(dist);
			this.prev = new HashMap<>(prev);
			this.highlightedEntry = highlightedEntry;
			this.res = res.clone();
			res.clearHighlights();
		}

		/**
		 * Constructor for the final state oif the algorithm
		 * @param res the resulting shortest path
		 */
		DijkstrasState(Stack<Character> res) {
			super("• Algorithm finished");
			visited = null; frontier = null;
			dist = null; prev = null;
			this.res = res;
		}

		@Override
		public Object getResult() {
			return res;
		}

		@SuppressWarnings("SuspiciousNameCombination")
		@Override
		public List<StateComponent> getComponents() {
			if (visited == null) { // If visited set to null (i.e. this is the final step)
				return List.of(); // Return empty list so no components will be visualized
			} else {
				// Represents both the dist and prev maps in a table format
				JPanel distTable = new JPanel(new GridBagLayout());
				{
					final int BORDER_WIDTH = 1;
					GridBagConstraints gbc = new GridBagConstraints();
					gbc.fill = GridBagConstraints.BOTH;
					gbc.ipadx = 10;
					gbc.ipady = 4;

					// Table headings
					JLabel label = new JLabel("Node", SwingConstants.CENTER);
					label.setBorder(BorderFactory.createLineBorder(Color.BLACK, BORDER_WIDTH));
					distTable.add(label, gbc);

					gbc.gridx = 1;
					label = new JLabel("From", SwingConstants.CENTER);
					label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
					distTable.add(label, gbc);

					gbc.gridx = 2;
					label = new JLabel("Total Distance", SwingConstants.CENTER);
					label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
					distTable.add(label, gbc);

					@SuppressWarnings("SuspiciousMethodCalls")
					List<Character> entries = this.dist.keySet().stream()
							.filter(node -> !this.visited.contains(node))
							.sorted(Comparator.comparing(node -> this.dist.get(node)).thenComparing(node -> (Character) node))
							.collect(Collectors.toList());
					for (int i = 0; i < entries.size(); i++) {
						gbc.gridy = i + 1;

						gbc.gridx = 0;
						label = new JLabel(entries.get(i).toString(), SwingConstants.CENTER);
						if (highlightedEntry != null && entries.get(i) == highlightedEntry) {
							label.setBackground(getOptions().HIGHLIGHT_COLOR);
							label.setOpaque(true);
						}
						label.setBorder(BorderFactory.createMatteBorder(0, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						distTable.add(label, gbc);

						gbc.gridx = 1;
						label = new JLabel(this.prev.get(entries.get(i)).toString(), SwingConstants.CENTER);
						if (highlightedEntry != null && entries.get(i) == highlightedEntry) {
							label.setBackground(getOptions().HIGHLIGHT_COLOR);
							label.setOpaque(true);
						}
						label.setBorder(BorderFactory.createMatteBorder(0, 0 , BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						distTable.add(label, gbc);

						gbc.gridx = 2;
						label = new JLabel(this.dist.get(entries.get(i)).toString(), SwingConstants.CENTER);
						if (highlightedEntry != null && entries.get(i) == highlightedEntry) {
							label.setBackground(getOptions().HIGHLIGHT_COLOR);
							label.setOpaque(true);
						}
						label.setBorder(BorderFactory.createMatteBorder(0, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						distTable.add(label, gbc);
					}
				}

				return List.of(new StateComponent<>("Visited set", visited) {
					/**
					 * The visited set is displayed as a simple label listing the visited nodes
					 * @see DijkstrasState#visited
					 */
					@Override
					public JComponent getJComponent() {
						JPanel ret = new JPanel();
						ret.setBackground(Color.WHITE);
						ret.add(new JLabel(name + ": " + component.stream().sorted().map(Object::toString).collect(Collectors.joining(", "))));
						return ret;
					}
				}, new StateComponent<>("Frontier set", frontier) {
					/**
					 * The frontier set is displayed as a simple label listing the known nodes
					 * @see DijkstrasState#frontier
					 */
					@Override
					public JComponent getJComponent() {
						JPanel ret = new JPanel();
						ret.add(new JLabel(name + ": " + component.stream().sorted().map(Object::toString).collect(Collectors.joining(", "))));
						return ret;
					}
				}, new StateComponent<>("Distance table", dist) {
					/**
					 * Represents the dist map in a table format
					 * @see DijkstrasState#dist
					 */
					@Override
					public JComponent getJComponent() {
						return distTable;
					}
				}, new StateComponent<>("Distance table", prev) {
					/**
					 * Represents the prev map in a table format
					 * @see DijkstrasState#prev
					 */
					@Override
					public JComponent getJComponent() {
						return distTable;
					}
				});
			}
	}

		@SuppressWarnings("unchecked")
		@Override
		synchronized void generateImages() {
			stateImages = new BufferedImage[2];

			if (res instanceof DirectedGraph) { // If this is an intermediate state
				Color gray25 = new Color(0x3F000000, true);
				{ // Graph image
					Map<String, Color> bindings = new HashMap<>();
					// Fade the unseen nodes
					graph.nodes().stream().filter(n -> !this.visited.contains(n) && !this.frontier.contains(n)).forEach(c -> {
						if (!graphView.isHighlighted(c)) {
							bindings.put("n" + c, gray25);
							bindings.put("v" + c, gray25);
							bindings.put("f" + c, new Color((getOptions().NODE_COLOR.getRGB() & 0xFFFFFF) | 0x3F000000, true));
						}
					});
					// Fade the unseen edges
					this.unseenEdges.forEach(e -> {
						bindings.put("e" + e.nodeU() + e.nodeV(), gray25);
						bindings.put("w" + e.nodeU() + e.nodeV(), gray25);
					});
					// Colour outline of start node
					bindings.put("n" + source, new Color((SOURCE_NODE_COLOR.getRGB() & 0xFFFFFF) | (bindings.getOrDefault("n" + source, SOURCE_NODE_COLOR).getAlpha() << 24), true));
					// Colour outline of end node
					bindings.put("n" + target, new Color((TARGET_NODE_COLOR.getRGB() & 0xFFFFFF) | (bindings.getOrDefault("n" + target, TARGET_NODE_COLOR).getAlpha() << 24), true));

					stateImages[0] = Common.dot(graphView, false, bindings);
				}
				{ // Result image
					Map<String, Color> bindings = new HashMap<>();
					frontier.forEach(c -> {
						bindings.put("n" + c, gray25);
						bindings.put("v" + c, gray25);
						bindings.put("f" + c, new Color(((((DirectedGraph) res).isHighlighted(c) ? getOptions().RESULT_HIGHLIGHT_COLOR :
								getOptions().RESULT_NODE_COLOR).getRGB() & 0xFFFFFF) | 0x3F000000, true));
						bindings.put("e" + prev.get(c) + c, ((DirectedGraph) res).isHighlighted(prev.get(c), c) ?
								new Color((getOptions().RESULT_HIGHLIGHT_COLOR.getRGB() & 0xFFFFFF) | 0x3F000000, true) : gray25);
						bindings.put("w" + prev.get(c) + c, ((DirectedGraph) res).isHighlighted(prev.get(c), c) ?
								new Color((getOptions().RESULT_HIGHLIGHT_COLOR.getRGB() & 0xFFFFFF) | 0x3F000000, true) : gray25);
					});
					// Colour outline of start node
					bindings.put("n" + source, new Color((SOURCE_NODE_COLOR.getRGB() & 0xFFFFFF) | (bindings.getOrDefault("n" + source, SOURCE_NODE_COLOR).getAlpha() << 24), true));

					stateImages[1] = Common.dot((Graph) res, true, bindings);
				}
			} else { // If this is the final state
				stateImages[0] = Common.dot(graphView);

				DirectedGraph g = new DirectedGraph();
				Stack<Character> result = (Stack<Character>) this.res;
				for (int i = result.size() - 1; i >= 1 ; i--) {
					g.addEdge(result.get(i), result.get(i - 1), graph.edgeValue(result.get(i), result.get(i - 1)).get());
				}
				stateImages[1] = Common.dot(g, true);
			}
			this.notify(); // Signals any thread waiting for the images to be generated that it is safe to resume
		}
	}

	/**
	 * A set of all edges the algorithm hasn't "seen". This is used for visualization purposes and is not necessary
	 * for the algorithm's execution.
	 */
	private Set<EndpointPair<Character>> unseenEdges;

	/** The start node */
	private Character source;
	/** The end node */
	private Character target;

	/** A set of all nodes that the algorithm has finished processing */
	private Set<Character> visited;
	/** A set of all nodes that the algorithm "knows" about and can select from at the next stage */
	private Set<Character> frontier;
	/** A map for each node int the {@link #frontier} set to the shortest distance known to it from the start node */
	private Map<Character, Integer> dist;
	/** A map for each node in the {@link #frontier} set to the previous node on the shortest path known to it from the start node */
	private Map<Character, Character> prev;

	/** A visual representation of the paths explored by the algorithm used for the intermediate result */
	private DirectedGraph res;

	/**
	 * Executes Dijkstra's shortest path algorithm on {@code graph}
	 * @param source the starting node
	 * @param target the end node
	 */
	public DijkstrasShortestPath(Graph graph, Character source, Character target) {
		super(graph, source, target);
	}

	@Override
	protected State initialize(Object... params) {
		source = (Character) params[0]; target = (Character) params[1];
		// Create the initial state  and initialize components
		DijkstrasState ret = new DijkstrasState();
		visited = new HashSet<>(ret.visited);
		frontier = new HashSet<>(ret.frontier);
		dist = new HashMap<>(ret.dist);
		prev = new HashMap<>(ret.prev);
		res = ((DirectedGraph) ret.res).clone();
		res.clearHighlights();
		unseenEdges = new HashSet<>(ret.unseenEdges);
		return ret;
	}

	@Override
	protected List<DijkstrasState> apply(State initialState) {
		List<DijkstrasState> ret = new ArrayList<>();
		ret.add((DijkstrasState) initialState); // Add initial state to the list of steps

		while (!frontier.isEmpty()) { // While there are unexplored nodes
			// Select node from the frontier set based on distance from start
			@SuppressWarnings("SuspiciousMethodCalls")
			Character c = Collections.min(frontier, Comparator.comparing(v -> dist.get(v)).thenComparing(v -> (Character) v));

			graph.setHighlight(prev.get(c), c); res.setHighlight(prev.get(c), c);
			graph.setHighlight(c); res.setHighlight(c);
			ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
					"• Select the closest node from the frontier set", c));

			frontier.remove(c); // Remove the node from the frontier set
			visited.add(c); // Add the node to the visited set

			graph.setHighlight(prev.get(c), c); res.setHighlight(prev.get(c), c);
			graph.setHighlight(c); res.setHighlight(c);
			ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
					"• Move " + c + " to visited set\n" +
							(c == target ? "• " + c + " is the target node!" : "• Start evaluating successors of " + c + " not yet visited"), null));
			if (c == target) break; // If the selected node is the end node the algorithm is finished
			// For every successor of the node that is not in the visited set
			for (Character v: graph.successors(c).stream().filter(s -> !visited.contains(s)).collect(Collectors.toSet())) {
				unseenEdges.remove(graph.isDirected() ? EndpointPair.ordered(c, v) : EndpointPair.unordered(c, v));
				graph.setHighlight(c); res.setHighlight(c);
				graph.setHighlight(c, v); res.setHighlight(c, v);
				graph.setHighlight(v); res.setHighlight(v);
				ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
						"• Evaluate node " + v + " from " + c + "'s successors", null));
				if (frontier.contains(v)) { // If it has been "seen" before
					int vdist = dist.get(c) + graph.edgeValue(c, v).get(); // Calculate new distance from start

					graph.setHighlight(c, v);
					graph.setHighlight(prev.get(v), v); res.setHighlight(prev.get(v), v);
					graph.setHighlight(v); res.setHighlight(v);
					ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
							"• New path of length " + vdist + " has been found to " + v + " from " + c +"\n" +
							"• Comparing to old path of length " + dist.get(v) + " from " + prev.get(v), v));

					if (vdist < dist.get(v)) { // If the new distance is shorter than previously known distance
						dist.replace(v, vdist);

						Character prevV;
						res.removeEdge(prevV = prev.replace(v, c), v);
						res.addEdge(c, v, graph.edgeValue(c, v).get());

						graph.setHighlight(c); res.setHighlight(c);
						graph.setHighlight(c, v); res.setHighlight(c, v);
						graph.setHighlight(v); res.setHighlight(v);
						ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
								"• The distance to " + v + " from " + c + " was less than the previously known distance from " + prevV + " and is updated", v));
					} else { // If new distance is longer than previously known distance
						graph.setHighlight(prev.get(v)); res.setHighlight(prev.get(v));
						graph.setHighlight(prev.get(v), v); res.setHighlight(prev.get(v), v);
						graph.setHighlight(v); res.setHighlight(v);
						ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
								"• The distance to " + v + " from " + c + " is " + (vdist != dist.get(v) ? "more than" : "equal to") +
								" the previously know distance from " + prev.get(v) + " and is ignored", v));
					}
				} else { // If the node hasn't been seen before
					// Add it to the frontier set and to dist table
					frontier.add(v);
					dist.put(v, dist.get(c) + graph.edgeValue(c, v).get());
					prev.put(v, c);

					res.addEdge(c, v, graph.edgeValue(c, v).get());

					graph.setHighlight(c); res.setHighlight(c);
					graph.setHighlight(c, v); res.setHighlight(c, v);
					graph.setHighlight(v); res.setHighlight(v);
					ret.add(new DijkstrasState(unseenEdges, visited, frontier, dist, prev, res,
							"• Previously unknown node " + v + " is added to the frontier set and the table is updated", v));
				}
			}
		}
		Stack<Character> res = getResult();
		ret.add(new DijkstrasState(res)); // Add final state
		res.forEach(node -> {
			graph.setHighlight(node);
			if (prev.containsKey(node)) graph.setHighlight(prev.get(node), node);
		});

		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DijkstrasState> getSteps() {
		return (List<DijkstrasState>) super.getSteps();
	}

	/** @return the shortest path from {@link #source} to {@link #target} */
	@Override
	public Stack<Character> getResult() {
		Stack<Character> ret = new Stack<>();
		Character c = target;
		do {
			ret.push(c);
			c = prev.get(c);
		} while (c != null);
		return ret;
	}
}
