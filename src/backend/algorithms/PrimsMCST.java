package backend.algorithms;

import GUI.utility.JPanel;
import backend.Common;
import backend.graphs.MCST;
import backend.graphs.UndirectedGraph;
import com.google.common.graph.EndpointPair;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static backend.Common.getOptions;

/** Implements Prim's minimum cost spanning tree algorithm */
@SuppressWarnings("ConstantConditions")
public class PrimsMCST extends Algorithm {

	/** @see Algorithm#getName() */
	public static final String NAME = "Prim's Algorithm";

	/** Represents the internal state of Prim's algorithm at a specific step of its execution */
	public class PrimsState extends State {

		/** A map that associates each node with the shortest known edge to it */
		private Map<Character, Map.Entry<EndpointPair<Character>, Integer>> nodeTable;
		/** A set of all unvisited nodes */
		private Set<Character> unvisited;

		/** Intermediate resulting minimum cost spanning tree */
		private MCST mcst;

		/**
		 * Constructor for the initial state of the algorithm
		 * @param startNode the initial node of the minimum cost spanning tree
		 */
		PrimsState(char startNode) {
			super("• Add the starting node " + startNode + " to the MCST\n" +
					"• Initialize the set of unvisited nodes to include all but " + startNode + "\n" +
					"• Initialize the closest node table");
			graphView.setHighlight(startNode);
			nodeTable = new LinkedHashMap<>(graph.nodes().size() - 1);
			// Initialize the set of unvisited nodes to include all but the starting node
			unvisited = new LinkedHashSet<>(graph.nodes());
			unvisited.remove(startNode);
			// Initialize the closest node table
			unvisited.forEach(node -> {
				if (graph.hasEdgeConnecting(startNode, node))
					nodeTable.put(node, new AbstractMap.SimpleEntry<>(EndpointPair.unordered(startNode, node), graph.edgeValue(startNode, node).get()));
				else
					nodeTable.put(node, new AbstractMap.SimpleEntry<>(null, Integer.MAX_VALUE));
			});
			// Initialize the minimum cost spanning tree to include the starting node
			mcst = new MCST(graph.nodes().size());
			mcst.addNode(startNode);
			mcst.setHighlight(startNode);
		}

		/** Constructor for intermediate states of the algorithm */
		PrimsState(Map<Character, Map.Entry<EndpointPair<Character>, Integer>> nodeTable, Set<Character> unvisited,
		           MCST mcst, String message) {
			super(message); // Set description of the step and highlighted node and/or edge
			this.nodeTable = new LinkedHashMap<>(nodeTable); // Copy the closest node table
			this.unvisited = new LinkedHashSet<>(unvisited); // Copy the set of unvisited nodes
			this.mcst = mcst.clone(); // Copy the minimum cost spanning tree
			graphView.highlightedNodes().forEach(node -> this.mcst.setHighlight(node));
			graphView.highlightedEdges().forEach(edge -> this.mcst.setHighlight(edge));
		}

		/**
		 * Constructor for the final state of the algorithm
		 * @param mcst the resulting minimum cost spanning tree
		 */
		PrimsState(MCST mcst) {
			super("• Algorithm finished");
			nodeTable = null; unvisited = null;
			this.mcst = mcst;
		}

		@Override
		public MCST getResult() {
			return mcst;
		}

		@Override
		public List<StateComponent> getComponents() {
			if (unvisited == null && nodeTable == null) { // If both components set to null (i.e. this is the final step)
				return List.of(); // Return empty list so no components will be visualized
			} else {
				return List.of(new StateComponent<>("Unvisited Set", unvisited) {
					/**
					 * The unvisited set is displayed as a simple label listing the unvisited nodes
					 * @see PrimsState#unvisited
					 */
					@Override
					public JComponent getJComponent() {
						JPanel ret = new JPanel();
						ret.add(new JLabel(name + ": " + component.stream().map(Object::toString).sorted().collect(Collectors.joining(", "))));
						return ret;
					}
				}, new StateComponent<>("Nodes Table", nodeTable) {
					/** Width of the table borders */
					private final int BORDER_WIDTH = 1;

					/**
					 * The closest node table is displayed as a table with 3 columns. The first is the node, the second
					 * is the shortest known edge to that node and the third is the length of that edge.
					 * @see PrimsState#nodeTable
					 */
					@SuppressWarnings("SuspiciousNameCombination")
					@Override
					public JComponent getJComponent() {
						JPanel table = new JPanel(new GridBagLayout());
						GridBagConstraints gbc = new GridBagConstraints();
						gbc.fill = GridBagConstraints.BOTH;
						gbc.ipadx = 10;
						gbc.ipady = 4;

						// Table headings
						gbc.gridx = 0; gbc.gridy = 0;
						JLabel label = new JLabel("Node", SwingConstants.CENTER);
						label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						table.add(label, gbc);

						gbc.gridx = 1;
						label = new JLabel("Edge", SwingConstants.CENTER);
						label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						table.add(label, gbc);

						gbc.gridx = 2;
						label = new JLabel("Distance", SwingConstants.CENTER);
						label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						table.add(label, gbc);

						// List of the table entries sorted by the edge distance and alphabetical order if distance is equal for any two
						List<Map.Entry<Character, Map.Entry<EndpointPair<Character>, Integer>>> entries = component.entrySet().stream().sorted(
								(o1, o2) -> o1.getValue().getValue().equals(o2.getValue().getValue()) ? o1.getKey().compareTo(o2.getKey()) :
										o1.getValue().getValue().compareTo(o2.getValue().getValue())).collect(Collectors.toList());

						for (int row = 1; row - 1 < entries.size(); row++) {
							gbc.gridy = row;

							gbc.gridx = 0;
							label = new JLabel(Character.toString(entries.get(row - 1).getKey()), SwingConstants.CENTER);
							if (entries.get(row - 1).getValue().getKey() != null && graphView.isHighlighted(entries.get(row - 1).getValue().getKey())) {
								label.setBackground(getOptions().HIGHLIGHT_COLOR);
								label.setOpaque(true);
							}
							label.setBorder(BorderFactory.createMatteBorder(0, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
							table.add(label, gbc);

							if (entries.get(row - 1).getValue().getKey() == null) { // If an edge to the node is not yet known
								gbc.gridx = 1;
								label = new JLabel("N/A", SwingConstants.CENTER);
								label.setBorder(BorderFactory.createMatteBorder(0, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
								table.add(label, gbc);

								gbc.gridx = 2;
								label = new JLabel("+∞", SwingConstants.CENTER);
								label.setBorder(BorderFactory.createMatteBorder(0, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
								table.add(label, gbc);
							} else {
								gbc.gridx = 1;
								label = new JLabel(entries.get(row - 1).getValue().getKey().nodeU() + " -- " + entries.get(row - 1).getValue().getKey().nodeV(), SwingConstants.CENTER);
								if (graphView.isHighlighted(entries.get(row - 1).getValue().getKey())) {
									label.setBackground(getOptions().HIGHLIGHT_COLOR);
									label.setOpaque(true);
								}
								label.setBorder(BorderFactory.createMatteBorder(0, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
								table.add(label, gbc);

								gbc.gridx = 2;
								label = new JLabel(entries.get(row - 1).getValue().getValue().toString(), SwingConstants.CENTER);
								if (graphView.isHighlighted(entries.get(row - 1).getValue().getKey())) {
									label.setBackground(getOptions().HIGHLIGHT_COLOR);
									label.setOpaque(true);
								}
								label.setBorder(BorderFactory.createMatteBorder(0, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
								table.add(label, gbc);
							}
						}

						return table;
					}
				});
			}
		}

		@Override
		synchronized void generateImages() {
			stateImages = new BufferedImage[2];
			stateImages[0] = Common.dot(graphView); // The graph and highlighted node and/or edge for this step
			// The minimum cost spanning tree at this step
			stateImages[1] = Common.dot(mcst);
			this.notify(); // Signals any thread waiting for the images to be generated that it is safe to resume
		}
	}

	/** A map that associates each node with the shortest known edge to it */
	private Map<Character, Map.Entry<EndpointPair<Character>, Integer>> nodeTable;
	/** A set of all unvisited nodes */
	private Set<Character> unvisited;

	/**
	 * The resulting minimum cost spanning tree of the algorithm's execution
	 * @see #getResult()
	 */
	private MCST mcst;

	/**
	 * <p>Executes Prim's minimum cost spanning tree algorithm on {@code graph}</p>
	 * <p><i>Note: the graph must be undirected</i></p>
	 */
	public PrimsMCST(UndirectedGraph graph) {
		super(graph);
	}

	/**
	 * <p>Executes Prim's algorithm on {@code graph}</p>
	 * <p><i>Note: the graph must be undirected</i></p>
	 * @param startNode the initial node from which the algorithm starts
	 */
	public PrimsMCST(UndirectedGraph graph, Character startNode) {
		super(graph, startNode);
	}

	@Override
	protected PrimsState initialize(Object... params) {
		// Create the initial state using a random starting node or a provided one (if it is found in the graph)
		@SuppressWarnings("SuspiciousMethodCalls")
		PrimsState ret = new PrimsState(params.length == 0 || !graph.nodes().contains(params[0]) ?
				graph.nodes().stream().skip((int) (Math.random() * graph.nodes().size())).findAny().get() : (Character) params[0]);
		nodeTable = new LinkedHashMap<>(ret.nodeTable);
		unvisited = new LinkedHashSet<>(ret.unvisited);
		mcst = ret.mcst.clone();
		mcst.clearHighlights();
		return ret;
	}

	@Override
	protected List<PrimsState> apply(State initialState) {
		List<PrimsState> ret = new ArrayList<>();
		ret.add((PrimsState) initialState); // Add initial state to the list of steps

		while (!unvisited.isEmpty()) { // While there are unvisited nodes
			// Select the next node by looking up the shortest edge in the table
			Map.Entry<Character, Map.Entry<EndpointPair<Character>, Integer>> nextNode = nodeTable.entrySet().stream().min(Comparator.comparing(entry -> entry.getValue().getValue())).get();
			graph.setHighlight(nextNode.getKey());
			graph.setHighlight(nextNode.getValue().getKey());
			ret.add(new PrimsState(nodeTable, unvisited, mcst, "• Find closest node")); // Add step

			// Add selected node and edge to the minimum cost spanning tree
			mcst.addEdge(nextNode.getKey(), (nextNode.getKey().equals(nextNode.getValue().getKey().nodeU()) ? nextNode.getValue().getKey().nodeV() : nextNode.getValue().getKey().nodeU()), nextNode.getValue().getValue());
			unvisited.remove(nextNode.getKey()); // Remove selected node from the unvisited set
			nodeTable.remove(nextNode.getKey()); // Remove selected node from the node table
			graph.setHighlight(nextNode.getKey());
			graph.setHighlight(nextNode.getValue().getKey());
			// Add step
			ret.add(new PrimsState(nodeTable, unvisited, mcst, "• Add node " + nextNode.getKey() + " and edge " +
					nextNode.getKey() + " -" + nextNode.getValue().getValue() + "-> " + (nextNode.getKey().equals(nextNode.getValue().getKey().nodeU()) ? nextNode.getValue().getKey().nodeV() : nextNode.getValue().getKey().nodeU()) +
					" to the MCST\n• Remove node " + nextNode.getKey() + " from the unvisited set and from the closest node table"));

			PrimsState newState;
			if (!unvisited.isEmpty()) { // If the selected node was not the last in the unvisited set
				// Using the neighbours of the selected node update the closest node table
				graph.adjacentNodes(nextNode.getKey()).stream().filter(unvisited::contains).
						filter(node -> graph.edgeValue(nextNode.getKey(), node).get() < nodeTable.get(node).getValue()).
						forEach(node -> nodeTable.put(node, new AbstractMap.SimpleEntry<>(EndpointPair.unordered(nextNode.getKey(), node), graph.edgeValue(nextNode.getKey(), node).get())));
				newState = new PrimsState(nodeTable, unvisited, mcst, "• Update the closest node table using the neighbours of " + nextNode.getKey());
			} else { // If this was the last step in the algorithm
				newState = new PrimsState(mcst); // Create final state
			}
			ret.add(newState); // Add step
		}

		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrimsState> getSteps() {
		return (List<PrimsState>) super.getSteps();
	}

	/**
	 * @return a minimum cost spanning tree of {@link #graph}
	 * @see #mcst
	 */
	@Override
	public MCST getResult() {
		return mcst;
	}
}
