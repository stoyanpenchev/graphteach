package backend.algorithms;

import backend.graphs.Graph;
import backend.graphs.GraphView;
import com.google.common.graph.EndpointPair;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/** Abstract base class for graph algorithms */
public abstract class Algorithm {

	/**
	 * Returns the name of this algorithm. For this method to work properly the algorithm needs to have a {@code public
	 * static [final] String} field called {@code NAME} declared. If it does not, the class name is returned instead.
	 */
	public final String getName() {
		try {
			return (String) this.getClass().getField("NAME").get(null);
		} catch (NoSuchFieldException | IllegalAccessException | ClassCastException e) {
			return this.getClass().getSimpleName();
		}
	}

	/** Abstract class representing the internal state of the algorithm at a specific step of its execution */
   public abstract class State {

    	/**
	     * Abstract wrapper class representing a component used in the algorithm's execution (e.g. a set of unvisited nodes)
	     * @param <T> the type of the component
	     */
	    public abstract class StateComponent<T> {
		    protected T component;
		    protected String name;

		    /** Wrap {@code component} and associate it with {@code name} */
		    StateComponent(String name, T component) {
			    this.name = name;
			    this.component = component;
		    }

		    /** @return the name of the component */
		    public String getName() {
			    return name;
		    }

		    /** @return the wrapped component */
		    public T getComponent() {
			    return component;
		    }

		    /** @return the class of the wrapped component */
		    @SuppressWarnings("unchecked")
		    public Class<T> getComponentClass() {
		    	return (Class<T>) component.getClass();
		    }

		    @Override
		    public String toString() {
			    return name + ": " + component.toString();
		    }

		    /** @return a JComponent to be used for visualization of the component */
		    public abstract JComponent getJComponent();
	    }

	    /**
	     * A description the current step of the algorithm
	     * @see #getMessage()
	     */
		protected String message;
		/**
		 * A view of the graph at the current step of the algorithm
		 * @see #getGraphView()
		 */
		protected GraphView graphView;
		/**
		 * <p>Array containing the images associated with this state.</p><br>
		 * <p>The array always has at least 2 elements. The element at index 0 is an image of the graph with the
		 * current step's associated highlighted nodes and/or edges. The element at index 1 is an image of the
		 * intermediate result of the algorithm (e.g. a minimum cost spanning tree). The rest of the elements, if any,
		 * are images associated with the state's components.</p>
		 * @see #generateImages()
		 * @see #getStateImages()
		 */
		protected BufferedImage[] stateImages;

		/**
		 * @param message a description the current step of the algorithm
		 */
		State(String message) {
			graphView = graph.getView(true);
			graph.clearHighlights();
			this.message = message;
		}

		/** @return the graph view associated with this state */
	    public GraphView getGraphView() {
		    return graphView;
	    }

	    /** @see #message */
	    public String getMessage() {
		    return message;
	    }

		/**
		 * @return a set of all nodes highlighted at this step
		 * @see GraphView#highlightedNodes()
		 */
		public Set<Character> getHighlightedNodes() {
			return graphView.highlightedNodes();
		}

		/**
		 * @return a set of all edges highlighted at this step
		 * @see GraphView#highlightedEdges()
		 */
		public Set<EndpointPair<Character>> getHighlightedEdges() {
			return graphView.highlightedEdges();
		}

	    /**
	     * @return an array of images associated with the state
	     * @see #stateImages
	     * @see #generateImages()
	     */
	    synchronized public BufferedImage[] getStateImages() {
		    try {
			   if (stateImages == null) this.wait(); // Causes the calling thread to be suspended until the drawThread can call generateImages()
		    } catch (InterruptedException e) {
		    	e.printStackTrace();
	        }
		    return stateImages;
	    }

	    /** @return the intermediate result associated with this state (e.g. a minimum cost spanning tree) */
	    public abstract Object getResult();

	    /**
	     * @return a list of the algorithm's components associated with this state
	     * @see StateComponent
	     */
		public abstract List<StateComponent> getComponents();

		/**
		 * Method to be called by {@link Algorithm#drawThread} in order to generate the images associated with this state
		 * @see #stateImages
		 */
	    abstract void generateImages();
    }

	/** A thread to asynchronously generate the images associated with each step of the algorithm */
	private static Thread drawThread = null;

    /** The graph the algorithm is being executed on */
    protected Graph graph;
    /** The original view of the graph before the algorithm has executed */
    private GraphView graph0;
    /**
     * A list of copies of the algorithm's internal state at each step of its execution
     * @see #getSteps()
     */
    protected List<? extends State> steps;

	/** Executes the algorithm on {@code graph} */
    protected Algorithm(Graph graph, Object... params) {
    	this.graph = graph;
    	graph0 = this.graph.getView(true);
    	this.graph.clearHighlights();
    	State initialState = initialize(params);
    	steps = apply(initialState);
    	generateImages();
    }

    /**
     * Method used to setup the initial state of the algorithm. Any components the algorithm uses should be initialized
     * in this method.
     * @return the initial state of the algorithm
     * @see State.StateComponent
     * @param params initialization parameters for the algorithm
     */
    protected abstract State initialize(Object... params);

	/**
	 * This method carries out the algorithm's execution and records the internal state at each step
	 * @param initialState the initial state of the algorithm (i.e. the result of {@link #initialize(Object...)})
	 * @return a list of State objects for every step of the algorithm's execution (i.e. the value to be associated with {@link #steps})
	 * @see State
	 */
	protected abstract List<? extends State> apply(State initialState);

	/**
	 * Starts a thread to asynchronously generate the images associated with each step of the algorithm
	 * @see #drawThread
	 * @see State#generateImages()
	 */
    public void generateImages() {
    	Runnable imageGen = () -> {
		    Iterator<? extends State> it = steps.iterator();
		    while (!drawThread.isInterrupted() && it.hasNext()) {
		    	it.next().generateImages();
		    }
	    };
    	if (drawThread == null) (drawThread = new Thread(imageGen)).start();
    	else if (!drawThread.isAlive()) {
    		steps.forEach(step -> step.stateImages = null);
		    (drawThread = new Thread(imageGen)).start();
	    } else {
    		drawThread.interrupt();
		    steps.forEach(step -> step.stateImages = null);
		    (drawThread = new Thread(imageGen)).start();
	    }
    }

    /** @return the result of the algorithm's execution (e.g. a minimum cost spanning tree) */
    public abstract Object getResult();

    /** @return a copy of the graph this algorithm was executed on */
	public final Graph getGraph() {
		Graph g = graph.clone();
		g.clearHighlights();
		graph0.highlightedNodes().forEach(g::setHighlight);
		graph0.highlightedEdges().forEach(g::setHighlight);
		return g;
	}

	/** @return {@link #steps} */
	public List<? extends State> getSteps() {
		return Collections.unmodifiableList(steps);
	}

	@Override
	public String toString() {
		return getName();
	}
}
