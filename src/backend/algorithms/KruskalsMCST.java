package backend.algorithms;

import GUI.utility.JPanel;
import GUI.utility.StadiumBorder;
import backend.Common;
import backend.graphs.MCST;
import backend.graphs.UndirectedGraph;
import backend.utility.DisjointSet;
import com.google.common.graph.EndpointPair;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static backend.Common.getOptions;

/** Implements Kruskal's minimum cost spanning tree algorithm */
public class KruskalsMCST extends Algorithm { // Kruskal

	/** @see Algorithm#getName() */
	public static final String NAME = "Kruskal's Algorithm";

	/** Represents the internal state of Kruskal's algorithm at a specific step of its execution */
	public class KruskalsState extends State {

		/**
		 * The subgraph(s) containing these endpoints will be highlighted. Can be {@code null}. This is used for
		 * visualization purposes and is not necessary for the algorithm's execution.
		 */
		private EndpointPair<Character> highlightedSubgraph;

		/**
		 * A set of connected subgraphs that exist at this step of the algorithm's execution. This is used to check if
		 * any cycles would be introduced in the MCST.
		 * @see DisjointSet
		 */
		private DisjointSet<Character> subgraphs;

		/** A map of the graph's edges sorted in ascending order by their weight */
		private LinkedHashMap<EndpointPair<Character>, Integer> edges;

		/** Intermediate resulting minimum cost spanning tree */
		private MCST mcst;

		/** Constructor for the initial state of the algorithm */
		KruskalsState() {
			super("• Sort edges by weight\n• Add each vertex to its own subgraph which will " +
					"later be used to check for cycles");
			// Initialize the subgraphs; at the initial state each node is in its own subgraph
			subgraphs = new DisjointSet<>(graph.nodes());
			highlightedSubgraph = null;
			// Create sorted edges map
			edges = graph.edges().entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toMap(
					Map.Entry::getKey, Map.Entry::getValue, (a , b) -> a, () -> new LinkedHashMap<>(graph.edges().size())));
			mcst = new MCST();
		}

		/** Constructor for intermediate states of the algorithm */
		KruskalsState(DisjointSet<Character> subgraphs, LinkedHashMap<EndpointPair<Character>, Integer> edges, MCST mcst,
		              String message, EndpointPair<Character> highlightedSubgraph) {
			super(message);
			this.subgraphs = new DisjointSet<>(subgraphs);
			this.highlightedSubgraph = highlightedSubgraph;
			this.edges = new LinkedHashMap<>(edges);
			this.mcst = mcst.clone();
			graphView.highlightedNodes().forEach(this.mcst::setHighlight);
			graphView.highlightedEdges().forEach(this.mcst::setHighlight);
		}

		/**
		 * Constructor for the final state of the algorithm
		 * @param mcst the resulting minimum cost spanning tree
		 */
		KruskalsState(MCST mcst) {
			super("• Algorithm finished");
			subgraphs = null; edges = null; highlightedSubgraph = null;
			this.mcst = mcst;
		}

		@Override
		public MCST getResult(){
			return mcst;
		}

		@Override
		public List<StateComponent> getComponents() {
			if (subgraphs == null && edges == null) { // If both components set to null (i.e. this is the final step)
				return List.of(); // Return empty list so no components will be visualized
			} else {
				return List.of(new StateComponent<>("Subgraphs", subgraphs) {
					/**
					 * The subgraphs are visualized as a series of sets of the nodes they contain
					 * @see KruskalsState#subgraphs
					 */
					@Override
					public JComponent getJComponent() {
						JPanel ret = new JPanel();
						ret.add(new JLabel(name + ":"));

						for (Set<Character> s : component) {
							StringJoiner str = new StringJoiner(", ");
							s.forEach(c -> str.add(c.toString()));
							JLabel subgraph = new JLabel(str.toString(), SwingConstants.CENTER);
							if (highlightedSubgraph != null && (subgraph.getText().contains(highlightedSubgraph.nodeU().toString()) ||
									subgraph.getText().contains(highlightedSubgraph.nodeV().toString()))) {
								subgraph.setBackground(getOptions().HIGHLIGHT_COLOR);
								subgraph.setOpaque(true);
							}
							subgraph.setBorder(StadiumBorder.createBlackStadiumBorder());
							ret.add(subgraph);
						}

						ret.getLayout().layoutContainer(ret);
						return ret;
					}
				}, new StateComponent<>("Edges", edges) {
					/** Width of the table borders */
					private final int BORDER_WIDTH = 1;

					/**
					 * The shortest edges are displayed in a table with 2 columns. The first one is the edge and second is its weight
					 * @see KruskalsState#edges
					 */
					@SuppressWarnings("SuspiciousNameCombination")
					@Override
					public JComponent getJComponent() {
						JPanel table = new JPanel(new GridBagLayout());
						GridBagConstraints gbc = new GridBagConstraints();
						gbc.fill = GridBagConstraints.BOTH;
						gbc.ipadx = 10;
						gbc.ipady = 4;

						// Table headings
						gbc.gridx = 0; gbc.gridy = 0;
						JLabel label = new JLabel("Edge", SwingConstants.CENTER);
						label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						table.add(label, gbc);

						gbc.gridx = 1;
						label = new JLabel("Distance", SwingConstants.CENTER);
						label.setBorder(BorderFactory.createMatteBorder(BORDER_WIDTH, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
						table.add(label, gbc);

						List<Map.Entry<EndpointPair<Character>, Integer>> entries = new ArrayList<>(component.entrySet());

						for (int row = 1; row - 1 < entries.size(); row++) {
							gbc.gridy = row;

							gbc.gridx = 0;
							label = new JLabel(entries.get(row - 1).getKey().nodeU() + " -- " + entries.get(row - 1).getKey().nodeV(), SwingConstants.CENTER);
							if (graphView.isHighlighted(entries.get(row - 1).getKey())) {
								label.setBackground(getOptions().HIGHLIGHT_COLOR);
								label.setOpaque(true);
							}
							label.setBorder(BorderFactory.createMatteBorder(0, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
							table.add(label, gbc);

							gbc.gridx = 1;
							label = new JLabel(entries.get(row - 1).getValue().toString(), SwingConstants.CENTER);
							if (graphView.isHighlighted(entries.get(row - 1).getKey())) {
								label.setBackground(getOptions().HIGHLIGHT_COLOR);
								label.setOpaque(true);
							}
							label.setBorder(BorderFactory.createMatteBorder(0, 0, BORDER_WIDTH, BORDER_WIDTH, Color.BLACK));
							table.add(label, gbc);
						}

						return table;
					}
				});
			}
		}

		@Override
		synchronized void generateImages() {
			stateImages = new BufferedImage[2];
			stateImages[0] = Common.dot(graphView); // The graph and highlighted node and/or edge for this step
			// The minimum cost spanning tree at this step
			stateImages[1] = Common.dot(mcst);
			this.notify(); // Signals any thread waiting for the images to be generated that it is safe to resume
		}
	}

	/**
	 * A set of connected subgraphs  used to check if any cycles would be introduced in the MCST.
	 * @see DisjointSet
	 */
	private DisjointSet<Character> subgraphs;

	/** A map of the graph's edges sorted in ascending order by their weight */
	private LinkedHashMap<EndpointPair<Character>, Integer> edges;

	/**
	 * The resulting minimum cost spanning tree of the algorithm's execution
	 * @see #getResult()
	 */
	private MCST mcst;

	/**
	 * <p>Executes Kruskal's minimum cost spanning tree algorithm on {@code graph}</p>
	 * <p><i>Note: the graph must be undirected</i></p>
	 */
	public KruskalsMCST(UndirectedGraph graph) {
		super(graph);
	}

	@Override
	protected State initialize(Object... params) {
		// Create the initial state and initialize components
		KruskalsState ret = new KruskalsState();
		subgraphs = new DisjointSet<>(ret.subgraphs);
		edges = new LinkedHashMap<>(ret.edges);
		mcst = ret.mcst.clone();
		return ret;
	}

	@Override
	protected List<KruskalsState> apply(State initialState) {
		List<KruskalsState> ret = new ArrayList<>();
		ret.add((KruskalsState) initialState); // Add initial state to the list of steps

		// For every edge
		for (Iterator<EndpointPair<Character>> it = edges.keySet().iterator(); it.hasNext() && mcst.edges().size() < graph.nodes().size() - 1; ) {
			EndpointPair<Character> edge = it.next(); // Select the shortest edge
			graph.setHighlight(edge);
			ret.add(new KruskalsState(subgraphs, edges, mcst, "• Find smallest edge\n• Check if adding edge introduces a cycle", edge));
			if (!subgraphs.inSameSet(edge.nodeU(), edge.nodeV())) { // Check if adding edge would introduce a cycle to the MCST
				mcst.addEdge(edge.nodeU(), edge.nodeV(), edges.get(edge)); // Add the edge
				graph.setHighlight(edge);
				ret.add(new KruskalsState(subgraphs, edges, mcst, "• Edge does not introduce a cycle and is added to the MCST", null));
				subgraphs.merge(edge.nodeU(), edge.nodeV()); // Merge the two subgraphs
				ret.add(new KruskalsState(subgraphs, edges, mcst, "• The two subgraphs containing the vertices of the edge are merged", edge));
			} else {
				// Edge would introduce a cycle and is discarded
				ret.add(new KruskalsState(subgraphs, edges, mcst, "• Edge would introduce a cycle and is skipped", null));
			}
			it.remove(); // The edge is removed from the list
			ret.add(new KruskalsState(subgraphs, edges, mcst, "• The edge is removed from the table", null));
		}
		ret.add(new KruskalsState(mcst)); // Add final state

		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<KruskalsState> getSteps() {
		return (List<KruskalsState>) super.getSteps();
	}

	/**
	 * @return a minimum cost spanning tree of {@link #graph}
	 * @see #mcst
	 */
	@Override
	public MCST getResult() {
		return mcst;
	}
}
