package GUI;

import GUI.utility.JPanel;

import javax.swing.*;
import java.awt.*;

import static GUI.GraphTeach.getApp;

/** Abstract base class for menus which are the top-level containers below the JFrame itself */
abstract class Menu extends JPanel {

	/** Reference to the menu before this one. Can be {@code null}, guaranteed to be {@code null} if this is {@link MainMenu}. */
	protected Menu previousMenu;

	/**
	 * Create new menu with {@link FlowLayout}
	 * @param previousMenu the previous menu displayed
	 */
	Menu(Menu previousMenu) {
		this(new FlowLayout(), previousMenu);
	}

	/**
	 * Create new menu with specified layout
	 * @param mgr the layout manager to use
	 * @param previousMenu the previous menu displayed
	 */
	Menu(LayoutManager mgr, Menu previousMenu) {
		super(mgr);
		this.previousMenu = previousMenu;
		// Add listeners to the menu that will change the size of the frame when the size of the menu changes
		// A synchronised thread is necessary because calls to getApp() before it is properly initialized could result in stack overflow
		new Thread(() -> {
			synchronized (GraphTeach.class) {
				addPropertyChangeListener("minimumSize", evt -> getApp().setMinimumSize((Dimension) evt.getNewValue()));
				addPropertyChangeListener("preferredSize", evt -> getApp().setPreferredSize((Dimension) evt.getNewValue()));
			}
		}).start();
		setKeyBindings(getActionMap(), getInputMap(WHEN_IN_FOCUSED_WINDOW));
	}

	/** Method for setting up active keybindings in this menu */
	protected void setKeyBindings(final ActionMap am, final InputMap im) {}
}
