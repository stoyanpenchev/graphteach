package GUI;

import GUI.utility.JPanel;
import GUI.utility.NodeChooser;
import backend.Common;
import backend.algorithms.DijkstrasShortestPath;
import backend.algorithms.KruskalsMCST;
import backend.algorithms.PrimsMCST;
import backend.graphs.Graph;
import backend.graphs.UndirectedGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import static GUI.GraphTeach.getApp;

/** Displays the loaded graph and available to execute algorithms */
class GraphMenu extends Menu {
	/** The graph that has been loaded */
	private Graph graph;
	/** An image of the loaded graph */
	private JLabel graphImage;

	/**
	 * Creates the graph menu
	 * @param graph the graph to be displayed
	 * @param previousMenu the menu displayed before the graph menu
	 */
	GraphMenu(Graph graph, Menu previousMenu) {
		super(new BorderLayout(), previousMenu);
		this.graph = graph;

		// Creates the image of the graph or throws GraphException if that fails
		add(graphImage = new JLabel(new ImageIcon(Common.dot(graph))), BorderLayout.CENTER);

		JPanel buttonPane = new JPanel(new GridBagLayout());
		add(buttonPane, BorderLayout.SOUTH);

		// Sets up "Prim's Algorithm" button
		JButton button = new JButton("Prim's Algorithm");
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = 2; gbc.insets = new Insets(0,2, 4, 2);
		if (!graph.isDirected()) { // Checks if the graph is undirected (Prim's algorithm can only be executed on undirected graphs)
			button.addActionListener(e -> {
				NodeChooser nodeChooser = new NodeChooser("Start Node");
				if (nodeChooser.showDialog() == NodeChooser.APPROVE_OPTION) {
					if (nodeChooser.getSelection(0) == null) getApp().algorithmDisplay(new PrimsMCST((UndirectedGraph) graph), this);
					else getApp().algorithmDisplay(new PrimsMCST((UndirectedGraph) graph, nodeChooser.getSelection(0)), this);
				}
			});
		} else { // If the graph is directed the button is disabled and an informative tooltip is added
			button.setEnabled(false);
			button.setToolTipText("Prim's algorithm cannot be applied to directed graphs");
		}
		buttonPane.add(button, gbc);

		// Sets up "Kruskal's Algorithm" button
		button = new JButton("Kruskal's Algorithm");
		gbc.gridx = 2;
		if (!graph.isDirected()) { // Checks if the graph is undirected (Kruskal's algorithm can only be executed on undirected graphs)
			button.addActionListener(e -> getApp().algorithmDisplay(new KruskalsMCST((UndirectedGraph) graph), this));
		} else { // If the graph is directed the button is disabled and an informative tooltip is added
			button.setEnabled(false);
			button.setToolTipText("Kruskal's algorithm cannot be applied to directed graphs");
		}
		buttonPane.add(button, gbc);

		// Sets up "Dijkstra's Algorithm" button
		button = new JButton("Dijkstra's Algorithm");
		gbc.gridx = 4;
		button.addActionListener(e -> {
			NodeChooser nodeChooser = new NodeChooser(true, "Source Node", "Target Node");
			if (nodeChooser.showDialog() == NodeChooser.APPROVE_OPTION) {
				getApp().algorithmDisplay(new DijkstrasShortestPath(graph, nodeChooser.getSelection(0), nodeChooser.getSelection(1)), this);
			}
		});
		buttonPane.add(button, gbc);

		// Sets up "Back" button
		button = new JButton("Back");
		gbc.gridx = 1; gbc.gridy = 1;
		button.addActionListener(e -> getApp().display(previousMenu));
		buttonPane.add(button, gbc);

		button = new JButton("Save");
		gbc.gridx = 3;
		button.addActionListener(new SaveHandler(graph, (BufferedImage) ((ImageIcon) graphImage.getIcon()).getImage()));
		buttonPane.add(button, gbc);

		((GridBagLayout) buttonPane.getLayout()).columnWeights = new double[] {0.5, 0.5, 0.125, 0.125, 0.5, 0.5};

		setMinimumSize(new Dimension(Math.max(420, graphImage.getIcon().getIconWidth()), graphImage.getIcon().getIconHeight() + 130));
		setPreferredSize(new Dimension(Math.max(500, graphImage.getIcon().getIconWidth()), Math.max(500, graphImage.getIcon().getIconHeight() + 130)));
	}

	/**
	 * Repaints the graph and if {@link #previousMenu} is an instance of {@link GraphCreator}, signals it to repaint its graph too.
	 * This method would be called if the graph display options were changed from an associated {@link AlgorithmDisplay}.
	 * @see AlgorithmDisplay#redraw()
	 * @see GraphCreator#updateGraph()
	 */
	void redraw() {
		graphImage.setIcon(new ImageIcon(Common.dot(graph)));
		graphImage.repaint();
		if (previousMenu instanceof GraphCreator) ((GraphCreator) previousMenu).updateGraph();
	}
}
