package GUI;

import GUI.utility.NodeChooser;
import backend.algorithms.Algorithm;
import backend.graphs.Graph;

import javax.swing.*;
import java.awt.*;

import static GUI.MainMenu.getMainMenu;

/** Main application singleton */
public class GraphTeach extends JFrame {

	/** The active application */
	private static GraphTeach APP;

	/**
	 * Initializes and returns the active application
	 * @return the active application
	 */
	public synchronized static GraphTeach getApp() {
		if (APP == null) APP = new GraphTeach();
		return APP;
	}

	/** Starts the application */
	private GraphTeach() {
		super("GraphTeach"); // Sets title
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		mainMenu(); // Creates the main menu and displays it

		setVisible(true);
	}

	/**
	 * Tries to display the specified menu
	 * @return {@code true} if the menu was successfully displayed
	 * @see Menu
	 */
	boolean display(Menu menu) {
		Container current = getContentPane();
		try {
			setContentPane(menu);
			setPreferredSize(menu.getPreferredSize());
			setMinimumSize(menu.getMinimumSize());
			if ((getExtendedState() & MAXIMIZED_BOTH) != MAXIMIZED_BOTH) pack();
			if (current instanceof AlgorithmDisplay && menu.previousMenu != current && !getTitle().equals("GraphTeach")) setTitle("GraphTeach");
			return true;
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			setContentPane(current);
			return false;
		} finally {
			revalidate();
			repaint();
		}
	}

	/**
	 * Displays the main menu
	 * @return {@code true} if the menu was successfully displayed
	 * @see MainMenu
	 */
	public boolean mainMenu() {
		return display(getMainMenu());
	}

	/**
	 * Display the graph creation menu
	 * @return {@code true} if the menu was successfully displayed
	 * @see GraphCreator
	 */
	public boolean graphCreation() {
		return display(new GraphCreator());
	}

	/**
	 * Displays the options menu
	 * @param previousMenu the menu displayed before the options menu
	 * @return {@code true} if the menu was successfully displayed
	 * @see OptionsMenu
	 */
	public boolean optionsMenu(Menu previousMenu) {
		return display(new OptionsMenu(previousMenu));
	}

	/**
	 * Display the graph menu, assumes previous menu is {@link MainMenu}
	 * @param graph the graph to be displayed
	 * @return {@code true} if the menu was successfully displayed
	 * @see GraphMenu
	 */
	public boolean graphMenu(Graph graph) {
		return graphMenu(graph, getMainMenu());
	}
	/**
	 * Display the graph menu
	 * @param graph the graph to be displayed
	 * @param previousMenu the menu displayed before the graph menu
	 * @return {@code true} if the menu was successfully displayed
	 * @see GraphMenu
	 */
	public boolean graphMenu(Graph graph, Menu previousMenu) {
		NodeChooser.NodeOptions.setChoices(graph.nodes());
		return display(new GraphMenu(graph, previousMenu));
	}

	/**
	 * Display the algorithm
	 * @param alg algorithm to be displayed
	 * @return {@code true} if the algorithm was successfully displayed
	 * @see AlgorithmDisplay
	 */
	public boolean algorithmDisplay(Algorithm alg) {
		return algorithmDisplay(alg, new GraphMenu(alg.getGraph(), getMainMenu()));
	}
	/**
	 * Display the algorithm
	 * @param alg algorithm to be displayed
	 * @param previousMenu the menu displayed before the algorithm menu
	 * @return {@code true} if the algorithm was successfully displayed
	 * @see AlgorithmDisplay
	 */
	public boolean algorithmDisplay(Algorithm alg, Menu previousMenu) {
		setTitle("GraphTeach: " + alg.getName());
		return display(new AlgorithmDisplay(alg, previousMenu));
	}
}
