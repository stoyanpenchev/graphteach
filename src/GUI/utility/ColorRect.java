package GUI.utility;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/** A simple component that displays a rectangle of a certain colour and size */
public class ColorRect extends JComponent {

	/** Create a new ColorRect with white color and size 25x25 */
	public ColorRect() {
		this(Color.WHITE, 25, 25);
	}

	/**
	 * Create a new ColorRect with the specified color and size 25x25
	 * @param color the color of the rectangle
	 */
	public ColorRect(Color color) {
		this(color, 25, 25);
	}

	/**
	 * Create a new ColorRect
	 * @param color the color of the rectangle
	 * @param width the width of the rectangle
	 * @param height the height of the rectangle
	 */
	public ColorRect(Color color, int width, int height) {
		setColor(color);
		setRectSize(new Dimension(width, height));
		setBorder(LineBorder.createBlackLineBorder());
	}

	@Override
	public void paintComponent(Graphics g0) {
		super.paintComponent(g0);
		Graphics2D g = (Graphics2D) g0;

		g.setColor(getForeground());
		g.fillRect(0, 0, getPreferredSize().width, getPreferredSize().height);
	}

	/**
	 * Sets the color of the rectangle
	 * @param color the new color of the rectangle
	 * @see #setForeground(Color)
	 */
	@Override
	public void setBackground(Color color) {
		setColor(color);
	}

	/**
	 * Sets the color of the rectangle
	 * @param color the new color of the rectangle
	 * @see #setBackground(Color)
	 */
	@Override
	public void setForeground(Color color) {
		setColor(color);
	}

	/**
	 * Sets the color of the rectangle
	 * @param color the new color of the rectangle
	 * @see #setForeground(Color)
	 * @see #setBackground(Color)
	 */
	private void setColor(Color color) {
		super.setBackground(color);
		super.setForeground(color);
		repaint();
	}

	@Override
	public void setPreferredSize(Dimension preferredSize) {
		setRectSize(preferredSize);
	}

	@Override
	public void setMaximumSize(Dimension maximumSize) {
		setRectSize(maximumSize);
	}

	/**
	 * Set the size of the rectangle
	 * @param d the new size of the rectangle
	 * @see #setPreferredSize(Dimension)
	 * @see #setMaximumSize(Dimension)
	 */
	private void setRectSize(Dimension d) {
		Dimension size = new Dimension(d);
		super.setPreferredSize(size);
		super.setMaximumSize(size);
	}
}
