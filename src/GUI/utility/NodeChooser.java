package GUI.utility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import static GUI.GraphTeach.getApp;

/** A dialog for selecting a number of nodes for algorithm initialization (e.g. a start node, an end node, etc.) */
@SuppressWarnings({"unchecked", "SuspiciousMethodCalls", "ConstantConditions"})
public class NodeChooser extends JDialog {

	/** A container class for a node that needs to be picked */
	public static class NodeOptions {
		/** All choices for nodes */
		private static Character choices[];
		/**
		 * Set the available node choices
		 * @throws IllegalArgumentException if {@code choices.size() == 0}
		 * @throws NullPointerException if any of the elements of {@code choices} is {@code null}
		 */
		public static void setChoices(Collection<Character> choices) {
			if (choices.size() == 0) throw new IllegalArgumentException("'choices' must not be empty");
			for (Character c: choices) if (c == null) throw new NullPointerException("'choices' can't contain null elements");
			NodeOptions.choices = choices.toArray(new Character[0]);
			Arrays.sort(NodeOptions.choices); // choices are sorted alphabetically
		}

		/** The description of the node choice (e.g "Start Node") */
		private String name;
		/** Is a random choice allowed for this node or does it have to be explicitly specified */
		private boolean randomAllowed;
		/** The selected choice for this node. if {@code randomAllowed && selected == null} the the choice of node will be random */
		private Character selected;

		/**
		 * Creates a node option with allowed random selection
		 * @param name a description of this node option (e.g "Start Node")
		 */
		public NodeOptions(String name) {
			this(name, true);
		}

		/**
		 * Creates a node option
		 * @param name a description of this node option (e.g "Start Node")
		 * @param randomAllowed is a random choice allowed for this node
		 */
		public NodeOptions(String name, boolean randomAllowed) {
			this.name = name;
			this.randomAllowed = randomAllowed;
			selected = randomAllowed ? null : choices[0];
		}
	}

	/** The user has pressed the OK button */
	public static final int APPROVE_OPTION = 0;
	/** The user has pressed the Cancel button or has closed the dialog */
	public static final int CANCEL_OPTION = 1;

	/** The node options that have to be chosen in this dialog */
	private NodeOptions nodes[];
	/** Combo boxes for the node options */
	private JComboBox comboBoxes[];
	/** Whether node selections must be exclusive to node options */
	private boolean exclusiveValues;
	/**
	 * The return value for the dialog
	 * @see #APPROVE_OPTION
	 * @see #CANCEL_OPTION
	 */
	private int returnValue;

	/**
	 * Creates the dialog with {@link #exclusiveValues} set to {@code false}
	 * @param nodes the descriptions for the node choices (e.g. "Start Node", "End Node", etc.)
	 */
	public NodeChooser(String... nodes) {
		this(false, nodes);
	}

	/**
	 * Creates the dialog
	 * @param exclusiveValues set {@code true} if node selections must be exclusive to node options
	 * @param nodes the descriptions for the node choices (e.g. "Start Node", "End Node", etc.)
	 */
	public NodeChooser(boolean exclusiveValues, String... nodes) {
		this(exclusiveValues, (NodeOptions[]) Arrays.stream(nodes).map(NodeOptions::new).toArray(NodeOptions[]::new));
	}

	/**
	 * Creates the dialog with {@link #exclusiveValues} set to {@code false}
	 * @param nodes the nodes that must be selected
	 */
	public NodeChooser(NodeOptions... nodes) {
		this(false, nodes);
	}

	/**
	 * Creates the dialog
	 * @param exclusiveValues set {@code true} if node selections must be exclusive to node options
	 * @param nodes the nodes that must be selected
	 * @throws IllegalArgumentException if {@link #exclusiveValues} is enabled but the number of choices is smaller than
	 * the number of options needed to be chosen; or if {@code nodes} is {@code null} or empty
	 */
	public NodeChooser(boolean exclusiveValues, NodeOptions... nodes) {
		super(getApp(), "Algorithm Options", true);
		if (exclusiveValues && NodeOptions.choices.length < nodes.length)
			throw new IllegalArgumentException("If exclusive choices are enabled, the number of choices can't be smaller than the number of nodes");
		if (nodes == null || nodes.length == 0) throw new IllegalArgumentException("'nodes' must not be null or empty");
		this.nodes = nodes;
		this.exclusiveValues = exclusiveValues;
		comboBoxes = new JComboBox[nodes.length];

		Container contentPanel = getContentPane();
		contentPanel.setBackground(Color.WHITE);

		JPanel nodesPanel = new JPanel();
		nodesPanel.setLayout(new BoxLayout(nodesPanel, BoxLayout.Y_AXIS));
		for (int i = 0; i < nodes.length; i++) { // For every node option
			NodeOptions no = nodes[i];
			JPanel nodePanel = new JPanel();
			nodePanel.setLayout(new BoxLayout(nodePanel, BoxLayout.X_AXIS));
			nodePanel.add(Box.createHorizontalStrut(10));
			nodePanel.add(new JLabel(no.name + ":"));
			nodePanel.add(new Box.Filler(new Dimension(25, 0), new Dimension(25, 0), new Dimension(Short.MAX_VALUE, 0)));

			JPanel optionsPanel = new JPanel();
			optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
			if (no.randomAllowed) { // If random choices are allowed for this option
				// Create "Random" radio button
				JRadioButton randomOption = new JRadioButton("Random", true);
				randomOption.setAlignmentX(LEFT_ALIGNMENT);
				optionsPanel.add(randomOption);

				// Create "Custom" radio button
				JPanel customPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
				customPanel.setAlignmentX(LEFT_ALIGNMENT);
				JRadioButton customOption = new JRadioButton("Custom");
				customPanel.add(customOption);
				customPanel.add(Box.createHorizontalStrut(10));
				// Create combo box for current node option
				Vector<Character> nodeChoices = new Vector<>(Arrays.asList(NodeOptions.choices));
				JComboBox<Character> nodesComboBox = new JComboBox<>(nodeChoices);
				nodesComboBox.setPreferredSize(new Dimension(45, 25));
				nodesComboBox.setMaximumSize(new Dimension(45, 25));
				nodesComboBox.setSelectedIndex(i);
				comboBoxes[i] = nodesComboBox;
				// Add item listener to combo box that handles exclusive values if necessary
				nodesComboBox.addItemListener(new ItemListener() {
					private Character oldSelection;

					@Override
					public void itemStateChanged(ItemEvent e) {
						if (nodesComboBox.isEnabled()) {
							if (e.getStateChange() == ItemEvent.DESELECTED) {
								oldSelection = (Character) e.getItem();
							} else {
								no.selected = (Character) e.getItem();
								if (exclusiveValues) {
									for (JComboBox comboBox: comboBoxes) {
										if (comboBox != nodesComboBox) {
											// Add old selection back in other combo boxes at the correct alphabetical place
											for (int j = 0; j < comboBox.getItemCount(); j++) {
												if (oldSelection < ((Character) comboBox.getItemAt(j))) {
													comboBox.insertItemAt(oldSelection, j);
													break;
												}
											}
											comboBox.removeItem(e.getItem()); // Remove the new selection from other combo boxes
										}
									}
								}
							}
						}
					}
				});
				nodesComboBox.setEnabled(false);
				customPanel.add(nodesComboBox);
				// Add item listener to the "Custom" radio button
				customOption.addItemListener(e -> {
					if (e.getStateChange() == ItemEvent.SELECTED) { // If "Custom" selected
						nodesComboBox.setEnabled(true);
						no.selected = (Character) nodesComboBox.getSelectedItem();
						if (exclusiveValues) {
							for (JComboBox comboBox: comboBoxes) {
								if (comboBox != nodesComboBox) {
									comboBox.removeItem(nodesComboBox.getSelectedItem()); // Remove the selected node from other combo boxes
								}
							}
						}
					} else { // If "Random" was selected
						nodesComboBox.setEnabled(false);
						no.selected = null;
						if (exclusiveValues) {
							for (JComboBox comboBox: comboBoxes) {
								if (comboBox != nodesComboBox) {
									// Add the selection at the correct alphabetical place in the other combo boxes
									for (int j = 0; j < comboBox.getItemCount(); j++) {
										if (((Character) nodesComboBox.getSelectedItem()) < ((Character) comboBox.getItemAt(j))) {
											comboBox.insertItemAt(nodesComboBox.getSelectedItem(), j);
											break;
										} else if (nodesComboBox.getSelectedItem() == comboBox.getItemAt(j)){
											break;
										}
									}
								}
							}
						}
					}
				});
				optionsPanel.add(customPanel);

				ButtonGroup nodeOptions = new ButtonGroup();
				nodeOptions.add(randomOption);
				nodeOptions.add(customOption);
			} else { // If random choices are not allowed for this option
				// Create the combo box for current node option
				Vector<Character> nodeChoices = new Vector<>(Arrays.asList(NodeOptions.choices));
				if (exclusiveValues) {
					for (int j = i + 1; j < nodes.length; j++) nodeChoices.remove(i + 1);
					for (int j = i - 1; j >= 0; j--) nodeChoices.remove(comboBoxes[j].getSelectedItem());
				}
				JComboBox<Character> nodesComboBox = new JComboBox<>(nodeChoices);
				nodesComboBox.setPreferredSize(new Dimension(45, 25));
				nodesComboBox.setMaximumSize(new Dimension(45, 25));
				comboBoxes[i] = nodesComboBox;
				// Add item listener to combo box that handles exclusive values if necessary
				nodesComboBox.addItemListener(new ItemListener() {
					private Character oldSelection;

					@Override
					public void itemStateChanged(ItemEvent e) {
						if (e.getStateChange() == ItemEvent.DESELECTED) {
							oldSelection = (Character) e.getItem();
						} else {
							no.selected = (Character) e.getItem();
							if (exclusiveValues) {
								for (JComboBox comboBox: comboBoxes) {
									if (comboBox != nodesComboBox) {
										// Add old selection back in other combo boxes at the correct alphabetical place
										for (int j = 0; j < comboBox.getItemCount(); j++) {
											if (oldSelection < ((Character) comboBox.getItemAt(j))) {
												comboBox.insertItemAt(oldSelection, j);
												break;
											}
										}
										comboBox.removeItem(e.getItem()); // Remove the selected node from other combo boxes
									}
								}
							}
						}
					}
				});
				optionsPanel.add(nodesComboBox);
			}

			nodePanel.add(optionsPanel);
			nodePanel.add(Box.createHorizontalStrut(10));
			nodesPanel.add(nodePanel);
			if (i != nodes.length - 1) nodesPanel.add(Box.createVerticalStrut(10));
		}

		contentPanel.add(nodesPanel, BorderLayout.CENTER);

		// Set up "OK" button
		JPanel buttonPanel = new JPanel();
		JButton okButton = new JButton("OK");
		okButton.addActionListener(e -> {
			returnValue = APPROVE_OPTION;
			if (exclusiveValues) {
				// Decide on values for node choices selected to be random
				List<Character> unsetNodes = Arrays.stream(NodeOptions.choices).filter(n -> {
					boolean keep = true;
					for (NodeOptions no: nodes) if (n.equals(no.selected)) keep = false;
					return keep;
				}).collect(Collectors.toList());
				for (NodeOptions no: nodes) if (no.selected == null) {
					int rand = (int) (Math.random() * unsetNodes.size());
					no.selected = unsetNodes.get(rand);
					unsetNodes.remove(rand);
				}
			}
			dispose();
		});
		buttonPanel.add(okButton);

		// Set up "Cancel" button
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(e -> {
			returnValue = CANCEL_OPTION;
			dispose();
		});
		buttonPanel.add(cancelButton);
		contentPanel.add(buttonPanel, BorderLayout.SOUTH);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				returnValue = CANCEL_OPTION;
			}
		});
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(getApp());
	}

	/**
	 * Displays the dialog
	 * @return either {@link #APPROVE_OPTION} or {@link #CANCEL_OPTION}
	 */
	public int showDialog() {
		setVisible(true);
		return returnValue;
	}

	/**
	 * Return the choice for the specified node option
	 * @param n the index of the option desired
	 * @return the choice for the option
	 * @throws IndexOutOfBoundsException if <code>n &gt;= {@link #nodes}.length</code>
	 */
	public Character getSelection(int n) {
		if (n > nodes.length) throw new IndexOutOfBoundsException();
		else return nodes[n].selected;
	}
}
