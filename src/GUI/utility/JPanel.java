package GUI.utility;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

/** An override of {@link javax.swing.JPanel} that sets the background to white and adds chaining support to keybindings */
public class JPanel extends javax.swing.JPanel {

	/** Creates a new JPanel with white background and {@link FlowLayout} */
	public JPanel() {
		this(new FlowLayout());
	}

	/**
	 * Create a new JPanel with a white background and the specified layout manager
	 * @param mgr - the layout manager to use
	 */
	public JPanel(LayoutManager mgr) {
		super(mgr);
		setBackground(Color.WHITE);
	}

	@Override
	protected void addImpl(Component comp, Object constraints, int index) {
		super.addImpl(comp, constraints, index);
		// Adds chaining support for keybindings
		if (comp instanceof JComponent && !(comp instanceof JTextComponent)) {
			((JComponent) comp).getInputMap().setParent(this.getInputMap());
			((JComponent) comp).getActionMap().setParent(this.getActionMap());
		}
	}
}
