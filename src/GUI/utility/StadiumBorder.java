package GUI.utility;

import javax.swing.border.AbstractBorder;
import java.awt.*;
import java.awt.geom.*;
import java.beans.ConstructorProperties;
import java.util.Objects;

/**
 * <p>A border in the shape of a <a href="https://en.wikipedia.org/wiki/Stadium_(geometry)">Stadium</a></p>
 * <p><img src="https://goodcalculators.com/i/stadium.png" alt="Stadium.png"></p>
 */
public class StadiumBorder extends AbstractBorder {

	private static StadiumBorder blackStadium;

	/**
	 * Convenience method for getting a black StadiumBorder of thickness 1
	 * @return a {@code StadiumBorder} with {@code Color.BLACK} and thickness 1
	 */
	public static StadiumBorder createBlackStadiumBorder() {
		if (blackStadium == null) {
			blackStadium = new StadiumBorder();
		}
		return blackStadium;
	}

	/**
	 *  The color of the border
	 * @see #getColor()
	 */
	private Color color;
	/**
	 * The color that should be painted outside the border
	 * @see #getBackground()
	 * @see #setBackground(Color)
	 */
	private Color bg = null;

	/**
	 * The thickness of the border
	 * @see #getThickness()
	 */
	private int thickness;

	/** Create a new stadium border of black color and thickness 1 */
	public StadiumBorder() {
		this(Color.BLACK, 1);
	}

	/**
	 * Create new stadium border with specified color and thickness 1
	 * @param color the color of the border
	 */
	public StadiumBorder(Color color) {
		this(color, 1);
	}

	/**
	 * Create a new stadium border of black color and specified thickness
	 * @param thickness the thickness of the border
	 */
	public StadiumBorder(int thickness) {
		this(Color.BLACK, thickness);
	}

	/**
	 * Create a new stadium border
	 * @param color the color of the border
	 * @param thickness the thickness of the border
	 */
	@ConstructorProperties({"color", "thickness"})
	public StadiumBorder(Color color, int thickness) {
		this.color = Objects.requireNonNull(color);
		this.thickness = thickness;
	}

	@Override
	public void paintBorder(Component c, Graphics g0, int x, int y, int width, int height) {
		if (thickness > 0 && g0 instanceof Graphics2D) {
			Graphics2D g = (Graphics2D) g0;

			Color color0 = g.getColor();
			g.setColor(Objects.requireNonNullElse(bg, c.getParent().getBackground()));

			if (width - height < 2) { // If true he border is drawn as a circle rather than a stadium
				if (thickness == 1) {
					width--;
					height--;

					Ellipse2D.Double circle = new Ellipse2D.Double(x, y, width, height);

					Path2D outside = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					outside.append(circle, false);
					Rectangle2D frame = circle.getFrame();
					frame.add(frame.getWidth() + 1, frame.getHeight() + 1);
					outside.append(frame, false);

					g.fill(outside);
					g.setColor(color);
					g.draw(circle);
				} else {
					Ellipse2D outer = new Ellipse2D.Double(x, y, width, height);
					Ellipse2D inner = new Ellipse2D.Double(x + thickness, y + thickness, width - 2 * thickness, height - 2 * thickness);

					Path2D circle = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					circle.append(outer, false);
					circle.append(inner, false);

					Path2D outside = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					circle.append(outer, false);
					Rectangle2D frame = outer.getFrame();
					frame.add(frame.getWidth() + 1, frame.getHeight() + 1);
					outside.append(frame, false);

					g.fill(outside);
					g.setColor(color);
					g.fill(circle);
				}
			} else {
				if (thickness == 1) {
					width--;
					height--;

					Path2D stadium = new Path2D.Double();
					stadium.append(new Line2D.Double(x + height / 2, y, x + width - height / 2, y), false);
					stadium.append(new Arc2D.Double(x + width - height, y, height, height, 90, -180, Arc2D.OPEN), true);
					stadium.append(new Line2D.Double(x + width - height / 2, y + height, x + height / 2, y + height), true);
					stadium.append(new Arc2D.Double(x, y, height, height, -90, -180, Arc2D.OPEN), true);
					stadium.closePath();

					Path2D outside = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					outside.append(stadium, false);
					Rectangle2D frame = stadium.getBounds2D();
					frame.add(frame.getWidth() + 1, frame.getHeight() + 1);
					outside.append(frame, false);

					g.fill(outside);
					g.setColor(color);
					g.draw(stadium);
				} else {
					Path2D outer = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					Path2D inner = new Path2D.Double(Path2D.WIND_EVEN_ODD);

					outer.append(new Line2D.Double(x + height / 2d, y, x + width - height, y), false);
					outer.append(new Arc2D.Double(x + width - height, y, height, height, 90, -180, Arc2D.OPEN), true);
					outer.append(new Line2D.Double(x + width - height, y + height, x + height / 2d, y + height), true);
					outer.append(new Arc2D.Double(x, y, height, height, 90, 180, Arc2D.OPEN), true);
					outer.closePath();

					inner.append(new Line2D.Double(x + height / 2d + thickness, y + thickness, x + width - height + thickness, y + thickness), false);
					inner.append(new Arc2D.Double(x + width - height + thickness, y + thickness, height - 2 * thickness, height - 2 * thickness, 90, -180, Arc2D.OPEN), true);
					inner.append(new Line2D.Double(x + width - height + thickness, y + height - thickness, x + height / 2d + thickness, y + height - thickness), true);
					inner.append(new Arc2D.Double(x + thickness, y + thickness, height - 2 * thickness, height - 2 * thickness, -90, -180, Arc2D.OPEN), true);
					inner.closePath();

					Path2D stadium = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					stadium.append(outer, false);
					stadium.append(inner, false);

					Path2D outside = new Path2D.Double(Path2D.WIND_EVEN_ODD);
					outside.append(outer, false);
					Rectangle2D frame = outer.getBounds2D();
					frame.add(frame.getWidth() + 1, frame.getHeight() + 1);
					outside.append(frame, false);

					g.fill(outside);
					g.setColor(color);
					g.fill(stadium);
				}
			}

			g.setColor(color0);
		}
	}

	@Override
	public Insets getBorderInsets(Component c, Insets insets) {
		insets.top = insets.bottom = thickness;
		insets.left = insets.right = thickness + c.getHeight() / 4;
		return insets;
	}

	/**
	 * Return the color that is to be painted outside the border. The default value is the background color of the component's parent;
	 * @see #setBackground(Color)
	 */
	public Color getBackground() {
		return bg;
	}

	/**
	 * Set the color that is to be painted outside the border
	 * @see #getBackground()
	 */
	public void setBackground(Color bg) {
		this.bg = bg;
	}

	/** @return the color of the border */
	public Color getColor() {
		return color;
	}

	/** @return the thickness of the border */
	public int getThickness() {
		return thickness;
	}

	@Override
	public boolean isBorderOpaque() {
		return true;
	}
}
