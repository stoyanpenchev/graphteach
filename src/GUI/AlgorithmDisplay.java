package GUI;

import GUI.utility.JPanel;
import backend.algorithms.Algorithm;
import backend.algorithms.Algorithm.State;
import backend.graphs.GraphView;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;

import static GUI.GraphTeach.getApp;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/** Displays the steps of an algorithm */
class AlgorithmDisplay extends Menu {

	/** Size for the navigation buttons */
	private static final int ICON_SIZE = 50;
	private static ImageIcon playImage, pauseImage, nextImage, previousImage, backImage, optionsImage, saveImage;
	// Initialize the navigation button icons
	static {
		try {
			playImage = new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/play.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
			pauseImage = new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/pause.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
			nextImage = new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/next.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
			previousImage = new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/previous.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
			backImage = new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/back.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
			optionsImage = new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/options.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
			saveImage =  new ImageIcon(ImageIO.read(AlgorithmDisplay.class.getResourceAsStream("/images/save.png")).getScaledInstance(ICON_SIZE, ICON_SIZE, Image.SCALE_SMOOTH));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** The algorithm that is being visualized */
	private Algorithm alg;
	/** Counter for the current step in the algorithm */
	private int currentState;

	private JPanel centerPanel, sidePanel, resultPanel, componentPanel;
	private JLabel graphImage, resultImage;
	private JTextArea messageTextArea;
	private JButton backButton, optionsButton, saveButton, autoButton, nextButton, previousButton;
	private JTextField currentStepField;

	/**
	 * Timer for autoplaying the algorithm's steps
	 * @see #startAutoStep()
	 * @see #stopAutoStep()
	 */
	private Timer stepTimer;

	/**
	 * Initializes the algorithm display
	 * @param alg algorithm to be displayed
	 * @param previousMenu the menu displayed before the algorithm menu
	 */
	@SuppressWarnings("deprecation")
	AlgorithmDisplay(Algorithm alg, Menu previousMenu) {
		super(previousMenu);
		GridBagLayout layout = new GridBagLayout();
		setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;

		this.alg = alg;
		currentState = 0;

		// Initialization of the timer with initial delay of 2 seconds
		stepTimer = new Timer(2000, e -> {
			if (currentState < alg.getSteps().size() - 1) next();
			else stopAutoStep();
		});

		JPanel titlePane = new JPanel();
		titlePane.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0 ,Color.BLACK));
		JLabel title = new JLabel(alg.getName());
		title.setFont(title.getFont().deriveFont(Font.BOLD).deriveFont(30f));
		titlePane.add(title);
		gbc.gridwidth = 3;
		add(titlePane, gbc);

		// Set up back button
		backButton = new JButton(backImage);
		backButton.setBackground(Color.WHITE);
		backButton.setToolTipText("Back");
		backButton.addActionListener(e -> getApp().display(previousMenu));
		gbc.gridwidth = 1; gbc.gridy = 1;
		add(backButton, gbc);

		// Set up options button
		optionsButton = new JButton(optionsImage);
		optionsButton.setBackground(Color.WHITE);
		optionsButton.setToolTipText("Options");
		optionsButton.addActionListener(e -> getApp().optionsMenu(this));
		gbc.gridy = 2;
		add(optionsButton, gbc);

		// Set up save button
		saveButton = new JButton(saveImage);
		saveButton.setBackground(Color.WHITE);
		saveButton.setToolTipText("Save...");
		saveButton.addActionListener(e -> {
			// Create a new dialog for choosing what to save
			JDialog dialog = new JDialog(getApp(), "Save...", true) {
				@Override
				protected JRootPane createRootPane() {
					JRootPane rootPanel = super.createRootPane();
					Container contentPanel = rootPanel.getContentPane();
					contentPanel.setBackground(Color.WHITE);

					// What to save: Graph/Components/Result/All
					JPanel optionPanel = new JPanel();
					((FlowLayout) optionPanel.getLayout()).setAlignOnBaseline(true);
					JPanel whatPanel = new JPanel();
					whatPanel.setLayout(new BoxLayout(whatPanel, BoxLayout.Y_AXIS));
					whatPanel.setBorder(BorderFactory.createTitledBorder(LineBorder.createBlackLineBorder(), "What", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP));
					JRadioButton graphButton = new JRadioButton("Graph");
					JRadioButton componentsButton = new JRadioButton("Components");
					if (currentState == alg.getSteps().size() - 1) componentsButton.setEnabled(false);
					JRadioButton resultButton = new JRadioButton("Result");
					JRadioButton allButton = new JRadioButton("All", true);
					ButtonGroup whatButtons = new ButtonGroup();
					whatButtons.add(graphButton);
					whatButtons.add(componentsButton);
					whatButtons.add(resultButton);
					whatButtons.add(allButton);
					whatButtons.getElements().asIterator().forEachRemaining(whatPanel::add);

					// Which step: Current/Last
					JPanel stepPanel = new JPanel();
					stepPanel.setLayout(new BoxLayout(stepPanel, BoxLayout.Y_AXIS));
					stepPanel.setBorder(BorderFactory.createTitledBorder(LineBorder.createBlackLineBorder(), "Step", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP));
					JRadioButton currentStep = new JRadioButton("Current", true);
					JRadioButton lastStep = new JRadioButton("Last");
					lastStep.addItemListener(ie -> {
						if (ie.getStateChange() == ItemEvent.SELECTED) {
							if (componentsButton.isSelected()) allButton.setSelected(true);
							componentsButton.setEnabled(false);
						} else if (currentState != alg.getSteps().size() - 1) {
							componentsButton.setEnabled(true);
						}
					});
					ButtonGroup stepButtons = new ButtonGroup();
					stepButtons.add(currentStep);
					stepButtons.add(lastStep);
					stepPanel.add(currentStep);
					stepPanel.add(lastStep);

					optionPanel.add(stepPanel);
					optionPanel.add(whatPanel);

					JPanel buttonPanel = new JPanel();

					// Set up "Save" button
					JButton saveButton = new JButton("Save");
					saveButton.addActionListener(e1 -> {
						GraphView graph = null;
						BufferedImage img = null;
						if (graphButton.isSelected()) { // If "Graph" is selected
							graph = alg.getSteps().get(currentStep.isSelected() ? currentState : (alg.getSteps().size() - 1)).getGraphView();
							img = alg.getSteps().get(currentStep.isSelected() ? currentState : (alg.getSteps().size() - 1)).getStateImages()[0];
						} else if (componentsButton.isSelected()) { // If "Components" is selected
							img = new BufferedImage(componentPanel.getWidth(), componentPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
							componentPanel.paint(img.createGraphics());
						} else if (resultButton.isSelected()) { // If "Result" is selected
							try {
								graph = (GraphView) alg.getSteps().get(currentStep.isSelected() ? currentState : (alg.getSteps().size() - 1)).getResult();
							} catch (ClassCastException ignored) {}
							img = alg.getSteps().get(currentStep.isSelected() ? currentState : (alg.getSteps().size() - 1)).getStateImages()[1];
						} else if (allButton.isSelected()) { // If "All" is selected
							if (currentStep.isSelected() || currentState == alg.getSteps().size() - 1) {
								img = new BufferedImage(centerPanel.getWidth() + sidePanel.getWidth(), centerPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
								Graphics2D g = img.createGraphics();
								centerPanel.paint(g);
								g.translate(centerPanel.getWidth(), 0);
								sidePanel.paint(g);
							} else {
								BufferedImage lastStateImages[] = alg.getSteps().get(alg.getSteps().size() - 1).getStateImages();
								img = new BufferedImage(lastStateImages[0].getWidth() + lastStateImages[1].getWidth(),
										Math.max(lastStateImages[0].getHeight(), lastStateImages[1].getHeight()), BufferedImage.TYPE_INT_RGB);
								Graphics2D g = img.createGraphics();
								g.setBackground(Color.WHITE);
								g.clearRect(0, 0, img.getWidth(), img.getHeight());
								g.drawImage(lastStateImages[0], 0, 0, null);
								g.setColor(Color.BLACK);
								g.drawString("• Algorithm finished", 5, lastStateImages[0].getHeight() + (img.getHeight() - lastStateImages[0].getHeight()) / 2);
								g.drawLine(lastStateImages[0].getWidth(), 0, lastStateImages[0].getWidth(), img.getHeight());
								g.drawImage(lastStateImages[1], lastStateImages[0].getWidth() + 1 , 0, null);
							}
						}
						setVisible(false);
						new SaveHandler(graph, img).actionPerformed(e);
						dispose();
					});
					buttonPanel.add(saveButton);

					// Set up "Cancel" button
					JButton cancelButton = new JButton("Cancel");
					cancelButton.addActionListener(e1 -> dispose());
					buttonPanel.add(cancelButton);

					contentPanel.add(optionPanel, BorderLayout.CENTER);
					contentPanel.add(buttonPanel, BorderLayout.SOUTH);

					return rootPanel;
				}
			};
			dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			dialog.pack();
			dialog.setResizable(false);
			dialog.setLocationRelativeTo(getApp());
			dialog.setVisible(true);
		});
		gbc.gridy = 3;
		add(saveButton, gbc);

		// Setting up the central panel that will display the graph and the description of the algorithm's steps
		centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
		centerPanel.add(Box.createVerticalGlue());
		JPanel graphPane = new JPanel();
		graphPane.add(graphImage = new JLabel());
		centerPanel.add(graphPane);
		centerPanel.add(Box.createVerticalGlue());
		messageTextArea = new JTextArea(5, 50);
		messageTextArea.setBackground(Color.WHITE);
		messageTextArea.setEditable(false);
		messageTextArea.setMaximumSize(new Dimension(300, 125));
		messageTextArea.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		messageTextArea.setLineWrap(true); messageTextArea.setWrapStyleWord(true);
		centerPanel.add(messageTextArea);
		gbc.gridx = 1; gbc.gridy = 1; gbc.gridheight = 4;
		add(centerPanel, gbc);

		// Setting up the side panel that will display the result of the algorithm and any components the algorithm may use
		sidePanel = new JPanel();
		sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));
		sidePanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.BLACK));
		gbc.gridx = 2;
		add(sidePanel, gbc);
		componentPanel = new JPanel();
		componentPanel.setLayout(new BoxLayout(componentPanel, BoxLayout.Y_AXIS));
		componentPanel.setPreferredSize(new Dimension(250, 250));
		componentPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 250));
		componentPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
		sidePanel.add(componentPanel, BorderLayout.NORTH);
		sidePanel.add(Box.createVerticalGlue());
		resultPanel = new JPanel();
		resultPanel.add(resultImage = new JLabel());
		sidePanel.add(resultPanel);
		sidePanel.add(Box.createVerticalGlue());

		// Setting up the bottom panel that will facilitate the navigation through the algorithm's steps
		JPanel bottomPane = new JPanel();
		bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.Y_AXIS));
		bottomPane.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));

		// Setting up "Previous" button
		JPanel buttonPane = new JPanel();
		previousButton = new JButton(previousImage);
		previousButton.setBackground(Color.WHITE);
		previousButton.addActionListener(e -> {
			// If shift is pressed when button is clicked the first step of the algorithm will be displayed; if not, the previous step will be displayed
			if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
				display(1);
				currentStepField.setText("1");
			}
			else previous();
		});
		previousButton.setToolTipText("Click while holding SHIFT to go to first step");
		buttonPane.add(previousButton);

		// Setting up "Play/Stop" button
		autoButton = new JButton(playImage);
		autoButton.setBackground(Color.WHITE);
		autoButton.addActionListener(e -> {
			if (currentState < alg.getSteps().size() - 1) {
				if (!stepTimer.isRunning()) startAutoStep();
				else stopAutoStep();
			}
		});
		// If the button is right clicked a dialog for setting up the delay will be displayed
		autoButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					autoButton.requestFocusInWindow();
					if (stepTimer.isRunning()) stepTimer.stop(); // Temporarily pause autostep
					String delay = JOptionPane.showInputDialog(getApp(), "Delay seconds:", stepTimer.getDelay() / 1000d);
					if (delay != null) {
						if (delay.matches("\\d+(\\.\\d+)?") && Double.parseDouble(delay) > 0)
							stepTimer.setDelay((int) (Double.parseDouble(delay) * 1000));
						else
							JOptionPane.showMessageDialog(getApp(), "That is not a valid delay!", "Error", JOptionPane.ERROR_MESSAGE);
					}
					if (autoButton.getIcon() == pauseImage) stepTimer.start(); // Resume autoplay if it was running
				}
			}
		});
		autoButton.setToolTipText("Right click to change delay");
		buttonPane.add(autoButton);

		// Setting up "Next" button
		nextButton = new JButton(nextImage);
		nextButton.setBackground(Color.WHITE);
		nextButton.addActionListener(e -> {
			// If shift is pressed when button is clicked the last step of the algorithm will be displayed; if not, the next step will be displayed
			if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
				display(alg.getSteps().size());
				currentStepField.setText(Integer.toString(alg.getSteps().size()));
			}
			else next();
		});
		nextButton.setToolTipText("Click while holding SHIFT to go to last step");
		buttonPane.add(nextButton);
		bottomPane.add(buttonPane);
		// Setting up exact step "go to" field
		JPanel stepsPane = new JPanel();
		stepsPane.add(currentStepField = new JTextField("1", 2));
		currentStepField.setHorizontalAlignment(SwingConstants.RIGHT);
		currentStepField.addActionListener(e -> {
			if (!currentStepField.getText().matches("\\d+") ||
					(Integer.parseInt(currentStepField.getText()) < 1 ||
							Integer.parseInt(currentStepField.getText()) > alg.getSteps().size())) {
				JOptionPane.showMessageDialog(getApp(), "Invalid value", "Error", JOptionPane.ERROR_MESSAGE);
				currentStepField.setText(Integer.toString(currentState + 1));
			} else {
				display(Integer.parseInt(currentStepField.getText()));
			}
			this.requestFocusInWindow();
		});
		JLabel totalSteps = new JLabel("/ " + alg.getSteps().size(), SwingConstants.LEFT);
		totalSteps.setBackground(Color.WHITE);
		totalSteps.setLabelFor(currentStepField);
		stepsPane.add(totalSteps);
		bottomPane.add(stepsPane);
		gbc.gridx = 0; gbc.gridy = 5;
		gbc.gridwidth = 3;
		add(bottomPane, gbc);

		displayState(alg.getSteps().get(currentState)); // Display the first step (currentState == 0)
		setMinimumSize(new Dimension(Math.max(600, graphImage.getIcon().getIconWidth() + 300), graphImage.getIcon().getIconHeight() + 350));
		setPreferredSize(new Dimension(Math.max(1000, graphImage.getIcon().getIconWidth() + 300), Math.max(900, graphImage.getIcon().getIconHeight() + 350)));
		layout.columnWidths = new int[] {50, Math.max(300, graphImage.getIcon().getIconWidth()), 250};
		layout.rowHeights = new int[] {50, 50, 50, 50, graphImage.getIcon().getIconHeight() - 150, 100};
		layout.columnWeights = new double[] {0, 0.5, 1};
		layout.rowWeights = new double[] {0, 0, 0, 0, 1, 0};
	}

	@Override
	protected void setKeyBindings(ActionMap am, InputMap im) {

		final AlgorithmDisplay AD = this;
		final int BUTTON_HOLD = 50; // In milliseconds

		// Left Arrow = Previous Step
		// Shift + Left Arrow = First Step
		final String PREVIOUS = "PREVIOUS";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), PREVIOUS);
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.SHIFT_DOWN_MASK), PREVIOUS); // FIRST
		am.put(PREVIOUS, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				previousButton.requestFocusInWindow();
				previousButton.doClick(BUTTON_HOLD);
			}
		});

		// Right Arrow = Next Step
		// Shift + Right Arrow = Last Step
		final String NEXT = "NEXT";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), NEXT);
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.SHIFT_DOWN_MASK), NEXT); // LAST
		am.put(NEXT, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nextButton.requestFocusInWindow();
				nextButton.doClick(BUTTON_HOLD);
			}
		});

		// Space = Play/Pause
		final String AUTO = "AUTO";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), AUTO);
		am.put(AUTO, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				autoButton.requestFocusInWindow();
				autoButton.doClick(BUTTON_HOLD);
			}
		});

		// Shift + Space = Set Delay
		final String DELAY = "DELAY";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, InputEvent.SHIFT_DOWN_MASK), DELAY);
		am.put(DELAY, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				autoButton.dispatchEvent(new MouseEvent(AD, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), 0,
						MouseInfo.getPointerInfo().getLocation().x - AD.getLocationOnScreen().x,
						MouseInfo.getPointerInfo().getLocation().y - AD.getLocationOnScreen().y,
						1, false, MouseEvent.BUTTON3));
			}
		});

		// Escape = Back
		final String BACK = "BACK";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), BACK);
		am.put(BACK, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				backButton.doClick(BUTTON_HOLD);
			}
		});

		// Ctrl + O = Options
		final String OPTIONS = "OPTIONS";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK), OPTIONS);
		am.put(OPTIONS, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				optionsButton.doClick(BUTTON_HOLD);
			}
		});

		// Ctrl + S = Save
		final String SAVE = "SAVE";
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK), SAVE);
		am.put(SAVE, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveButton.requestFocusInWindow();
				saveButton.doClick(BUTTON_HOLD);
			}
		});
	}

	/** Displays the specified {@code state} and its associated components */
	private void displayState(State state) {
		graphImage.setIcon(new ImageIcon(state.getStateImages()[0])); // Display graph associated with state
		resultImage.setIcon(new ImageIcon(state.getStateImages()[1])); // Display result associated with state
		resultPanel.setMaximumSize(new Dimension(resultImage.getIcon().getIconWidth(), resultImage.getIcon().getIconHeight()));
		messageTextArea.setText(state.getMessage()); // Display description of step

		if (!state.getComponents().isEmpty()) { // If there are any components to display
			if (sidePanel.getComponent(0) != componentPanel) { // Add the component panel if it was not there
				sidePanel.add(componentPanel, 0);
				sidePanel.revalidate();
			}
			componentPanel.removeAll(); // Clear old components from the component panel
			componentPanel.add(Box.createVerticalGlue());
			// Add components to the component panel
			state.getComponents().forEach(component -> {
				if (!Arrays.asList(componentPanel.getComponents()).contains(component.getJComponent())) {
					componentPanel.add(component.getJComponent());
					componentPanel.add(Box.createVerticalGlue());
				}
			});
			componentPanel.revalidate(); // Revalidate the panel's layout
		} else { // If there aren't any components to display
			sidePanel.remove(componentPanel); // Remove the component panel
			sidePanel.revalidate(); // Revalidate the side panel's layout
		}

		repaint(); // Repaint the window
	}

	/** Displays a specific {@code step} */
	public void display(int step) {
		if (step > 0 && step <= alg.getSteps().size()) {
			currentState = step - 1;
			displayState(alg.getSteps().get(currentState));
		}
	}

	/**
	 * Regenerates all images associated with the algorithm and if {@link #previousMenu} is an instance of {@link GraphMenu},
	 * signal it to repaint its graph too. This method would be called if the graph display options were changed.
	 * @see GraphMenu#redraw()
	 */
	void redraw() {
		alg.generateImages();
		displayState(alg.getSteps().get(currentState));
		if (previousMenu instanceof GraphMenu) ((GraphMenu) previousMenu).redraw();
	}

	/** Displays the next step if there is one */
	public void next() {
		if (currentState < alg.getSteps().size() - 1) {
			displayState(alg.getSteps().get(++currentState));
			currentStepField.setText(Integer.toString(currentState + 1));
		}
	}

	/** Displays the previous step if there is one */
	public void previous() {
		if (currentState > 0) {
			displayState(alg.getSteps().get(--currentState));
			currentStepField.setText(Integer.toString(currentState + 1));
		}
	}

	/**
	 * Starts autostep through the algorithm
	 * @see #stepTimer
	 * @see #stopAutoStep()
	 */
	public void startAutoStep() {
		autoButton.setIcon(pauseImage);
		autoButton.repaint();
		stepTimer.start();
	}

	/**
	 * Stops the autostep
	 * @see #stepTimer
	 * @see #startAutoStep()
	 */
	public void stopAutoStep() {
		autoButton.setIcon(playImage);
		autoButton.repaint();
		stepTimer.stop();
	}
}
