package GUI;

import GUI.utility.ColorRect;
import GUI.utility.JPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import static GUI.GraphTeach.getApp;
import static backend.Common.*;

/**
 * Manages the options for displaying graphs
 * @see backend.Common.Options Options
 */
class OptionsMenu extends Menu {

	private static JPanel nodeShape, nodeColor, highlightColor, sameOptions, resultNodeShape, resultNodeColor, resultHighlightColor;
	private static JComboBox<Options.NodeShape> nodeShapeComboBox, resultNodeShapeComboBox;
	private static ColorRect nodeColorSquare, highlightColorSquare, resultNodeColorSquare, resultHighlightColorSquare;
	private static JButton nodeColorChooseButton, highlightColorChooseButton, resultNodeColorChooseButton, resultHighlightColorChooseButton;
	private static JCheckBox sameOptionsCheckbox;
	static {
		// Node Shape
		nodeShape = new JPanel();
		nodeShape.setLayout(new BoxLayout(nodeShape, BoxLayout.X_AXIS));
		nodeShape.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		nodeShape.add(Box.createHorizontalStrut(25));
		JLabel optionLabel = new JLabel("Node Shape:");
		optionLabel.setBackground(Color.WHITE);
		nodeShape.add(optionLabel);
		nodeShape.add(Box.createHorizontalGlue());
		nodeShapeComboBox = new JComboBox<>(Options.NodeShape.values());
		optionLabel.setLabelFor(nodeShapeComboBox);
		nodeShape.add(nodeShapeComboBox);
		nodeShape.add(Box.createHorizontalStrut(25));


		// Node Colour
		nodeColor = new JPanel();
		nodeColor.setLayout(new BoxLayout(nodeColor, BoxLayout.X_AXIS));
		nodeColor.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		nodeColor.add(Box.createHorizontalStrut(25));
		optionLabel = new JLabel("Node Color:");
		optionLabel.setBackground(Color.WHITE);
		nodeColor.add(optionLabel);
		nodeColor.add(Box.createHorizontalGlue());
		nodeColorSquare = new ColorRect(getOptions().NODE_COLOR, 100, 25);
		nodeColor.add(nodeColorSquare);
		nodeColorChooseButton = new JButton("Choose...");
		nodeColor.add(nodeColorChooseButton);
		optionLabel.setLabelFor(nodeColorSquare);
		nodeColor.add(Box.createHorizontalStrut(25));

		// Highlight Colour
		highlightColor = new JPanel();
		highlightColor.setLayout(new BoxLayout(highlightColor, BoxLayout.X_AXIS));
		highlightColor.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		highlightColor.add(Box.createHorizontalStrut(25));
		optionLabel = new JLabel("Highlight Color:");
		optionLabel.setBackground(Color.WHITE);
		highlightColor.add(optionLabel);
		highlightColor.add(Box.createHorizontalGlue());
		highlightColorSquare = new ColorRect(getOptions().HIGHLIGHT_COLOR, 100, 25);
		highlightColor.add(highlightColorSquare);
		highlightColorChooseButton = new JButton("Choose...");
		highlightColor.add(highlightColorChooseButton);
		optionLabel.setLabelFor(highlightColorSquare);
		highlightColor.add(Box.createHorizontalStrut(25));

		// Same Options For Results
		sameOptions = new JPanel();
		sameOptions.setLayout(new BoxLayout(sameOptions, BoxLayout.X_AXIS));
		sameOptions.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		sameOptions.add(Box.createHorizontalStrut(25));
		optionLabel = new JLabel("Same Options for Results:");
		optionLabel.setBackground(Color.WHITE);
		sameOptions.add(optionLabel);
		sameOptions.add(Box.createHorizontalGlue());
		sameOptionsCheckbox = new JCheckBox("", getOptions().sameOptionsForResults());
		optionLabel.setLabelFor(sameOptionsCheckbox);
		sameOptions.add(sameOptionsCheckbox);
		sameOptions.add(Box.createHorizontalStrut(25));

		// Result Node Shape
		resultNodeShape = new JPanel();
		resultNodeShape.setLayout(new BoxLayout(resultNodeShape, BoxLayout.X_AXIS));
		resultNodeShape.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		resultNodeShape.add(Box.createHorizontalStrut(25));
		optionLabel = new JLabel("Result Node Shape:");
		optionLabel.setBackground(Color.WHITE);
		resultNodeShape.add(optionLabel);
		resultNodeShape.add(Box.createHorizontalGlue());
		resultNodeShapeComboBox = new JComboBox<>(Options.NodeShape.values());
		optionLabel.setLabelFor(resultNodeShapeComboBox);
		resultNodeShape.add(resultNodeShapeComboBox);
		resultNodeShape.add(Box.createHorizontalStrut(25));

		// Result Node Colour
		resultNodeColor = new JPanel();
		resultNodeColor.setLayout(new BoxLayout(resultNodeColor, BoxLayout.X_AXIS));
		resultNodeColor.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		resultNodeColor.add(Box.createHorizontalStrut(25));
		optionLabel = new JLabel("Result Node Color:");
		optionLabel.setBackground(Color.WHITE);
		resultNodeColor.add(optionLabel);
		resultNodeColor.add(Box.createHorizontalGlue());
		resultNodeColorSquare = new ColorRect(getOptions().RESULT_NODE_COLOR, 100, 25);
		resultNodeColor.add(resultNodeColorSquare);
		resultNodeColorChooseButton = new JButton("Choose...");
		resultNodeColor.add(resultNodeColorChooseButton);
		optionLabel.setLabelFor(resultNodeColorSquare);
		resultNodeColor.add(Box.createHorizontalStrut(25));

		// Result Highlight Colour
		resultHighlightColor = new JPanel();
		resultHighlightColor.setLayout(new BoxLayout(resultHighlightColor, BoxLayout.X_AXIS));
		resultHighlightColor.setMaximumSize(new Dimension(Integer.MAX_VALUE, 25));
		resultHighlightColor.add(Box.createHorizontalStrut(25));
		optionLabel = new JLabel("Result Highlight Color:");
		optionLabel.setBackground(Color.WHITE);
		resultHighlightColor.add(optionLabel);
		resultHighlightColor.add(Box.createHorizontalGlue());
		resultHighlightColorSquare = new ColorRect(getOptions().RESULT_HIGHLIGHT_COLOR, 100, 25);
		resultHighlightColor.add(resultHighlightColorSquare);
		resultHighlightColorChooseButton = new JButton("Choose...");
		resultHighlightColor.add(resultHighlightColorChooseButton);
		optionLabel.setLabelFor(resultHighlightColorSquare);
		resultHighlightColor.add(Box.createHorizontalStrut(25));
	}

	/** Removes any listeners from the static components so they can be used in a new instance */
	private static void sanitizeComponents() {
		for (ActionListener al: nodeShapeComboBox.getActionListeners()) nodeShapeComboBox.removeActionListener(al);
		for (ActionListener al: nodeColorChooseButton.getActionListeners()) nodeColorChooseButton.removeActionListener(al);
		for (ActionListener al: highlightColorChooseButton.getActionListeners()) highlightColorChooseButton.removeActionListener(al);

		for (ItemListener il: sameOptionsCheckbox.getItemListeners()) sameOptionsCheckbox.removeItemListener(il);

		for (ActionListener al: resultNodeShapeComboBox.getActionListeners()) resultNodeShapeComboBox.removeActionListener(al);
		for (ActionListener al: resultNodeColorChooseButton.getActionListeners()) resultNodeColorChooseButton.removeActionListener(al);
		for (ActionListener al: resultHighlightColorChooseButton.getActionListeners()) resultHighlightColorChooseButton.removeActionListener(al);
	}

	private Options oldOptions, newOptions;

	/**
	 * Creates the options menu
	 * @param previousMenu the menu displayed before the options menu
	 */
	OptionsMenu(Menu previousMenu) {
		super(new BorderLayout(), previousMenu);

		oldOptions = getOptions();
		newOptions = oldOptions.clone();
		sanitizeComponents();

		JPanel centerPane = new JPanel();
		centerPane.setLayout(new BoxLayout(centerPane, BoxLayout.Y_AXIS));
		centerPane.add(Box.createVerticalGlue());

		// Node Shape
		nodeShapeComboBox.setSelectedItem(oldOptions.NODE_SHAPE);
		nodeShapeComboBox.addActionListener(e -> {
			newOptions.NODE_SHAPE = (Options.NodeShape) nodeShapeComboBox.getSelectedItem();
			if (sameOptionsCheckbox.isSelected()) newOptions.RESULT_NODE_SHAPE = (Options.NodeShape) nodeShapeComboBox.getSelectedItem();
		});
		centerPane.add(nodeShape);
		centerPane.add(Box.createVerticalStrut(10));

		// Node Colour
		nodeColorSquare.setForeground(oldOptions.NODE_COLOR);
		nodeColorChooseButton.addActionListener(e -> {
			Color pickedColor = JColorChooser.showDialog(getApp(), "Choose Node Color", newOptions.NODE_COLOR);

			if (pickedColor != null) {
				newOptions.NODE_COLOR = pickedColor;
				if (sameOptionsCheckbox.isSelected()) newOptions.RESULT_NODE_COLOR = pickedColor;
			}
			nodeColorSquare.setForeground(newOptions.NODE_COLOR);
		});
		centerPane.add(nodeColor);
		centerPane.add(Box.createVerticalStrut(10));

		// Highlight Colour
		highlightColorSquare.setForeground(oldOptions.HIGHLIGHT_COLOR);
		highlightColorChooseButton.addActionListener(e -> {
			Color pickedColor = JColorChooser.showDialog(getApp(), "Choose Highlight Color", newOptions.HIGHLIGHT_COLOR);
			if (pickedColor != null) {
				newOptions.HIGHLIGHT_COLOR = pickedColor;
				if (sameOptionsCheckbox.isSelected()) newOptions.RESULT_HIGHLIGHT_COLOR = pickedColor;
			}
			highlightColorSquare.setForeground(newOptions.HIGHLIGHT_COLOR);
		});
		centerPane.add(highlightColor);

		// Same Options For Results
		sameOptionsCheckbox.setSelected(oldOptions.sameOptionsForResults());
		centerPane.add(Box.createVerticalStrut(10));
		centerPane.add(sameOptions);


		// Result Node Shape
		resultNodeShapeComboBox.addActionListener(e -> newOptions.RESULT_NODE_SHAPE = (Options.NodeShape) resultNodeShapeComboBox.getSelectedItem());
		if (!oldOptions.sameOptionsForResults()) {
			centerPane.add(Box.createVerticalStrut(10));
			centerPane.add(resultNodeShape);
		}

		// Result Node Colour
		resultNodeColorChooseButton.addActionListener(e -> {
			Color pickedColor = JColorChooser.showDialog(getApp(), "Choose Result Node Color", newOptions.RESULT_NODE_COLOR);
			if (pickedColor != null) newOptions.RESULT_NODE_COLOR = pickedColor;
			resultNodeColorSquare.setForeground(newOptions.RESULT_NODE_COLOR);
		});
		if (!oldOptions.sameOptionsForResults()) {
			centerPane.add(Box.createVerticalStrut(10));
			centerPane.add(resultNodeColor);
		}

		// Result Highlight Colour
		resultHighlightColorChooseButton.addActionListener(e -> {
			Color pickedColor = JColorChooser.showDialog(getApp(), "Choose Result Highlight Color", newOptions.RESULT_HIGHLIGHT_COLOR);
			if (pickedColor != null) newOptions.RESULT_HIGHLIGHT_COLOR = pickedColor;
			resultHighlightColorSquare.setForeground(newOptions.RESULT_HIGHLIGHT_COLOR);
		});
		if (!oldOptions.sameOptionsForResults()) {
			centerPane.add(Box.createVerticalStrut(10));
			centerPane.add(resultHighlightColor);
		}

		// Add item listener to same options checkbox that handles the adding and removing of result options
		sameOptionsCheckbox.addItemListener(e -> {
			if (e.getStateChange() == ItemEvent.DESELECTED) {
				newOptions.RESULT_NODE_SHAPE = (Options.NodeShape) resultNodeShapeComboBox.getSelectedItem();
				newOptions.RESULT_NODE_COLOR = resultNodeColorSquare.getForeground();
				newOptions.RESULT_HIGHLIGHT_COLOR = resultHighlightColorSquare.getForeground();
				centerPane.add(Box.createVerticalStrut(10), 8);
				centerPane.add(resultNodeShape, 9);
				centerPane.add(Box.createVerticalStrut(10), 10);
				centerPane.add(resultNodeColor, 11);
				centerPane.add(Box.createVerticalStrut(10), 12);
				centerPane.add(resultHighlightColor, 13);
				setMinimumSize(new Dimension(420, 400));
			} else {
				newOptions.RESULT_NODE_SHAPE = newOptions.NODE_SHAPE;
				newOptions.RESULT_NODE_COLOR = newOptions.NODE_COLOR;
				newOptions.RESULT_HIGHLIGHT_COLOR = newOptions.HIGHLIGHT_COLOR;
				for (int i = 0; i < 6; i++) centerPane.remove(8);
				setMinimumSize(new Dimension(420, 250));
			}
			centerPane.revalidate();
			centerPane.repaint();
		});

		centerPane.add(Box.createVerticalGlue());
		add(centerPane, BorderLayout.CENTER);

		JPanel buttonPane = new JPanel();

		// Set up "OK" button
		JButton okButton = new JButton("OK");
		okButton.addActionListener(e -> {
			if (!newOptions.equals(oldOptions)) {
				setOptions(newOptions);
				if (previousMenu instanceof GraphCreator) ((GraphCreator) previousMenu).updateGraph();
				if (previousMenu instanceof AlgorithmDisplay) ((AlgorithmDisplay) previousMenu).redraw();
			}
			getApp().display(previousMenu);
		});
		buttonPane.add(okButton);

		// Set up "Cancel" button
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(e -> getApp().display(previousMenu));
		buttonPane.add(cancelButton);

		//Set up "Reset" button
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(e -> {
			oldOptions = getOptions();
			newOptions = new Options();
			nodeShapeComboBox.setSelectedItem(newOptions.NODE_SHAPE);
			nodeColorSquare.setForeground(newOptions.NODE_COLOR);
			highlightColorSquare.setForeground(newOptions.HIGHLIGHT_COLOR);
			sameOptionsCheckbox.setSelected(newOptions.sameOptionsForResults());
			resultNodeShapeComboBox.setSelectedItem(newOptions.RESULT_NODE_SHAPE);
			resultNodeColorSquare.setForeground(newOptions.RESULT_NODE_COLOR);
			resultHighlightColorSquare.setForeground(newOptions.RESULT_HIGHLIGHT_COLOR);
		});
		buttonPane.add(resetButton);
		add(buttonPane, BorderLayout.SOUTH);

		setMinimumSize(new Dimension(420, 250));
		setPreferredSize(new Dimension(500, 500));
	}
}
