package GUI;

import backend.graphs.DirectedGraph;
import backend.graphs.Graph;
import backend.graphs.GraphException;
import backend.graphs.UndirectedGraph;
import com.google.common.graph.EndpointPair;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static GUI.GraphTeach.getApp;

/** Class for handling loading of graphs from different formats */
class LoadHandler implements ActionListener {
	
	private static JFileChooser fileChooser = new JFileChooser();
	static {
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("All Supported Files", "graph", "html", "gt"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("GRAPH Files", "graph"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("HTML Document", "html"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("GraphTeach File", "gt"));
	}

	private Graph graph;

	/** @throws RuntimeException if the caller could not be identified */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (loadGraph()) {
			Container callerMenu = ((JComponent) e.getSource()).getParent();
			while (!(callerMenu instanceof Menu)) callerMenu = callerMenu.getParent();
			if (callerMenu instanceof MainMenu) getApp().graphMenu(graph, (Menu) callerMenu);
			else if (callerMenu instanceof GraphCreator) ((GraphCreator) callerMenu).loadGraph(graph);
			else throw new RuntimeException("Unknown caller");
		}
	}

	/** Loads a graph from a .graph file, an HTML document or a .gt file */
	private boolean loadGraph() {
		try {
			if (fileChooser.showOpenDialog(getApp()) == JFileChooser.APPROVE_OPTION) {
				if (fileChooser.getSelectedFile().getName().endsWith(".graph")) { // GRAPH File
					boolean directed;

					try (BufferedReader file = new BufferedReader(new FileReader(fileChooser.getSelectedFile()))) {
						String line = file.readLine();
						while (line.startsWith("#")) line = file.readLine(); // Skip comment lines
						// Check if graph is directed
						if (!line.toLowerCase().matches("directed\\s*:\\s*(true|false)")) throw new GraphException("Incorrect graph file format!");
						directed = Boolean.parseBoolean(line.split(":\\s*")[1]);

						graph = directed ? new DirectedGraph() : new UndirectedGraph(); // Construct new graph

						line = file.readLine();
						while (line.startsWith("#")) line = file.readLine(); // Skip comment lines
						// Add nodes to graph
						if (!line.matches("((?i:nodes)\\h*:\\h*(\\d+|[A-Z](\\h*,\\h*[A-Z]){0,25})|[A-Z]\\h*-\\d+-" + (directed ? ">" : "") + "\\h*[A-Z])")) throw new GraphException("Incorrect graph file format!");
						if (line.matches("(?i:nodes)\\h*:\\h*\\d+")) { // e.g. nodes: 5
							graph.addNodes(Integer.parseInt(line.split(":\\h*")[1]));
						} else if (line.matches("(?i:nodes)\\h*:\\h*[A-Z](\\h*,\\h*[A-Z]){0,25}")) { // e.g. nodes: A, B, C
							Arrays.stream(line.split("\\h*[:,]\\h*")).skip(1).map(s -> s.charAt(0)).distinct().forEach(graph::addNode);
						}

						if (line.toLowerCase().startsWith("nodes")) line = file.readLine();
						// Add edges to graph
						while (line != null && !line.toLowerCase().startsWith("highlighted-")) {
							if (!line.startsWith("#")) { // Skip comment lines
								if (!line.matches("[A-Z]\\h*-\\d+-" + (directed ? ">" : "") + "\\h*[A-Z]"))
									throw new GraphException("Incorrect graph file format!");
								String edge[] = line.split("((?<!\\d)\\h*-|" + "-" + (directed ? ">" : "") + "\\h*)");
								graph.addEdge(edge[0].charAt(0), edge[2].charAt(0), Integer.parseInt(edge[1]));
							}
							line = file.readLine();
						}

						if (line != null && line.toLowerCase().startsWith("highlighted-nodes")) {
							if (!line.matches("(?i:highlighted-nodes)\\h*:\\h*[A-Z](\\h*,\\h*[A-Z]){0," + (graph.nodes().size() - 1) + "}")) throw new GraphException("Incorrect graph file format!");
							Arrays.stream(line.split("\\h*[:,]\\h*")).skip(1).map(s -> s.charAt(0)).distinct().filter(graph.nodes()::contains).forEach(graph::setHighlight);
							line = file.readLine();
						}

						while (line != null && line.startsWith("#")) line = file.readLine(); // Skip comment lines

						if (line != null && line.toLowerCase().startsWith("highlighted-edges")) {
							if (!line.matches("(?i:highlighted-edges)\\h*:\\h*[A-Z]\\h*" + (graph.isDirected() ? "->" : "-") +
									"\\h*[A-Z](\\h*;\\h*[A-Z]\\h*" + (graph.isDirected() ? "->" : "-") + "\\h*[A-Z]){0," + (graph.edges().size() - 1) + "}")) throw new GraphException("Incorrect graph file format!");
							Arrays.stream(line.split("\\h*[:;]\\h*")).skip(1).map(s -> s.split(graph.isDirected() ? "->" : "-"))
									.map(a -> graph.isDirected() ? EndpointPair.ordered(a[0].charAt(0), a[1].charAt(0)) : EndpointPair.unordered(a[0].charAt(0), a[1].charAt(0)))
									.filter(graph.edges()::containsKey).forEach(graph::setHighlight);
							line = file.readLine();
						}

						while (line != null && line.startsWith("#")) line = file.readLine(); // Skip comment lines

						if (line != null) throw new GraphException("Incorrect graph file format!");
					} catch (NullPointerException ex) { // If file.readLine() returns null prematurely
						throw new GraphException("Incorrect graph file format!");
					}
				} else if (fileChooser.getSelectedFile().getName().endsWith(".html")) { // HTML File
					Matcher matcher = Pattern.compile("(?m)\\Q<meta name=\"generator\" content=\"GraphTeach\"\\E\\s+" +
							"\\Qdata-directed=\"\\E(true|false)\"\\s+" +
							"\\Qdata-nodes=\"\\E((?:[1-9]|1\\d|2[0-6])|[A-Z](?:,[A-Z]){0,25})\"\\s+" +
							"\\Qdata-edges=\"\\E([A-Z]-\\d+->?[A-Z](?:;[A-Z]-\\d+->?[A-Z])*)\"\\s*" +
							"(?:\\Qdata-highlighted-\\E(nodes)=\"([A-Z](?:,[A-Z])*)\")?\\s*" +
							"(?:\\Qdata-highlighted-edges\\E=\"([A-Z]->?[A-Z](?:;[A-Z]->?[A-Z])*)\")?\\s*/>$")
							.matcher(new String(Files.readAllBytes(fileChooser.getSelectedFile().toPath()), StandardCharsets.UTF_8));
					if (matcher.find()) {
						boolean directed = Boolean.parseBoolean(matcher.group(1));
						graph = directed ? new DirectedGraph() : new UndirectedGraph();
						if (matcher.group(2).matches("\\d+")) {
							graph.addNodes(Integer.parseInt(matcher.group(2)));
						} else {
							Arrays.stream(matcher.group(2).split(",")).forEach(n -> graph.addNode(n.charAt(0)));
						}
						Arrays.stream(matcher.group(3).split(";")).map(edge -> edge.split("(-|->)"))
								.forEach(edge -> graph.addEdge(edge[0].charAt(0), edge[2].charAt(0), Integer.parseInt(edge[1])));
						if (matcher.group(4) != null) {
							if (matcher.group(4).equals("nodes")) {
								Arrays.stream(matcher.group(5).split(",")).forEach(n -> graph.setHighlight(n.charAt(0)));
								if (matcher.group(6) != null) Arrays.stream(matcher.group(6).split(";")).map(edge -> {
									if (edge.matches("[A-Z]" + (directed ? "->" : "-") + "[A-Z]")) {
										return edge.split("->?");
									} else throw new GraphException("Incorrect HTML document format!");
								}).forEach(edge -> graph.setHighlight(edge[0].charAt(0), edge[1].charAt(0)));
							} else {
								Arrays.stream(matcher.group(4).split(";")).map(edge -> {
									if (edge.matches("[A-Z]" + (directed ? "->" : "-") + "[A-Z]")) {
										return edge.split("->?");
									} else throw new GraphException("Incorrect HTML document format!");
								}).forEach(edge -> graph.setHighlight(edge[0].charAt(0), edge[1].charAt(0)));
							}
						}
					} else throw new GraphException("Incorrect HTML document format!");
				} else if (fileChooser.getSelectedFile().getName().endsWith(".gt")) { // GraphTeach File
					try (ObjectInputStream file = new ObjectInputStream(new FileInputStream(fileChooser.getSelectedFile()))) {
						graph = (Graph) file.readObject();
					} catch (Exception ex) {
						throw new GraphException("Error reading file!");
					}
				} else throw new GraphException("Unsupported file format!"); // Unknown File
				return true;
			}
		} catch (GraphException | IOException ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(getApp(), ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}

	/** @return the loaded graph */
	public Graph getGraph() {
		if (graph == null) loadGraph();
		return graph;
	}
}
