package GUI;

import GUI.utility.JPanel;
import backend.Common;
import backend.graphs.DirectedGraph;
import backend.graphs.Graph;
import backend.graphs.UndirectedGraph;
import com.google.common.graph.EndpointPair;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

import static GUI.GraphTeach.getApp;
import static GUI.MainMenu.getMainMenu;

/**
 * Creates graphs natively
 * @see Graph
 */
@SuppressWarnings("unchecked")
class GraphCreator extends Menu {

	private static ImageIcon addNodeImage, addNodeImagePressed, removeNodeImage, removeNodeImagePressed;
	static {
		try {
			addNodeImage = new ImageIcon(ImageIO.read(GraphCreator.class.getResourceAsStream("/images/play.png")).getScaledInstance(5, 10, Image.SCALE_SMOOTH));

			BufferedImage img = new BufferedImage(addNodeImage.getIconWidth(), addNodeImage.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = img.createGraphics();
			g.scale(-1, 1);
			g.drawImage(addNodeImage.getImage(), -addNodeImage.getIconWidth(), 0, null);
			g.dispose();
			removeNodeImage = new ImageIcon(img);

			AlphaComposite alphaMask = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);

			img = new BufferedImage(addNodeImage.getIconWidth(), addNodeImage.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
			g = img.createGraphics();
			g.drawImage(addNodeImage.getImage(), 0, 0, null);
			g.setComposite(alphaMask);
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, addNodeImage.getIconWidth(), addNodeImage.getIconHeight());
			g.dispose();
			addNodeImagePressed = new ImageIcon(img);
			
			img = new BufferedImage(removeNodeImage.getIconWidth(), removeNodeImage.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
			g = img.createGraphics();
			g.drawImage(removeNodeImage.getImage(), 0, 0, null);
			g.setComposite(alphaMask);
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, removeNodeImage.getIconWidth(), removeNodeImage.getIconHeight());
			g.dispose();
			removeNodeImagePressed = new ImageIcon(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** The graph that is being manipulated */
	private Graph graph;

	private JRadioButton undirectedButton, directedButton;
	private JLabel nodeCount;
	private DefaultListModel<Map.Entry<EndpointPair<Character>, Integer>> edgeListModel;
	private JComboBox<Character> sourceNodeComboBox, targetNodeComboBox;
	private JTextField edgeWeightField;

	/** The image of the graph being manipulated */
	private JLabel graphImage;

	/** Initializes the graph creator */
	@SuppressWarnings({"ConstantConditions"})
	GraphCreator() {
		super(new BorderLayout(), getMainMenu());
		setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));

		JPanel sidePanel = new JPanel();
		sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));
		sidePanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.BLACK));
		sidePanel.setPreferredSize(new Dimension(250, 0));

		Font headingFont;

		// Directed?
		JPanel component = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 5));
		component.setMaximumSize(new Dimension(250, 60));
		component.setPreferredSize(component.getMaximumSize());
		JLabel label = new JLabel("Graph:", SwingConstants.CENTER);
		label.setFont(headingFont = label.getFont().deriveFont(Font.BOLD, 16));
		label.setPreferredSize(new Dimension(200, 20));
		component.add(label);
		// Undirected
		undirectedButton = new JRadioButton("Undirected", true);
		undirectedButton.addItemListener(e -> {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				Map.Entry edges[] = new Map.Entry[edgeListModel.getSize()];
				edgeListModel.copyInto(edges);
				edgeListModel.removeAllElements();
				for (Map.Entry<EndpointPair<Character>, Integer> edge: edges)
					edgeListModel.addElement(new AbstractMap.SimpleEntry<>(EndpointPair.unordered(edge.getKey().nodeV(), edge.getKey().nodeU()), edge.getValue()));
				buildGraph();
				((JLabel) ((JPanel) sidePanel.getComponent(2)).getComponent(5)).setText("--");
			}
		});
		component.add(undirectedButton);
		// Directed
		directedButton = new JRadioButton("Directed");
		directedButton.addItemListener(e -> {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				Map.Entry edges[] = new Map.Entry[edgeListModel.getSize()];
				edgeListModel.copyInto(edges);
				edgeListModel.removeAllElements();
				for (Map.Entry<EndpointPair<Character>, Integer> edge: edges)
					edgeListModel.addElement(new AbstractMap.SimpleEntry<>(EndpointPair.ordered(edge.getKey().nodeU(), edge.getKey().nodeV()), edge.getValue()));
				buildGraph();
				((JLabel) ((JPanel) sidePanel.getComponent(2)).getComponent(5)).setText("->");
			}
		});
		component.add(directedButton);
		ButtonGroup isDirectedButtons = new ButtonGroup();
		isDirectedButtons.add(undirectedButton);
		isDirectedButtons.add(directedButton);
		sidePanel.add(component);


		// Nodes
		component = new JPanel(new FlowLayout(FlowLayout.CENTER, 8, 5));
		component.setMaximumSize(new Dimension(250, 90));
		component.setPreferredSize(component.getMaximumSize());
		label = new JLabel("Nodes:", SwingConstants.CENTER);
		label.setFont(headingFont);
		label.setPreferredSize(new Dimension(240, 20));
		component.add(label);
		// Remove Node Button
		JButton removeNode = new JButton(removeNodeImage);
		removeNode.setPreferredSize(new Dimension(10, 10));
		removeNode.setContentAreaFilled(false);
		removeNode.setBorderPainted(false);
		removeNode.addMouseListener(new MouseAdapter() {
			private boolean pressed = false;

			@Override
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && removeNode.contains(e.getPoint())) {
					pressed = true;
					removeNode.setIcon(removeNodeImagePressed);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				pressed = false;
				if (SwingUtilities.isLeftMouseButton(e) && removeNode.contains(e.getPoint())) {
					removeNode.setIcon(removeNodeImage);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				if (pressed) {
					removeNode.setIcon(removeNodeImagePressed);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				removeNode.setIcon(removeNodeImage);
			}
		});
		removeNode.addActionListener(e -> {
			int nc = Integer.parseInt(nodeCount.getText());
			char node;
			if (nc > 1 && graph.removeNode(node = Collections.max(graph.nodes()))) {
				nodeCount.setText(Integer.toString(--nc));
				sourceNodeComboBox.removeItem(node);
				targetNodeComboBox.removeItem(node);
				Map.Entry edges[] = new Map.Entry[edgeListModel.getSize()];
				edgeListModel.copyInto(edges);
				for (Map.Entry<EndpointPair<Character>, Integer> edge: edges) {
					if (edge.getKey().nodeU() == node || edge.getKey().nodeV() == node)
						edgeListModel.removeElement(edge);
				}
				updateGraph();
			}
		});
		component.add(removeNode);
		// Node Count Field
		nodeCount = new JLabel("1", SwingConstants.CENTER);
		nodeCount.setMaximumSize(new Dimension(15, 20));
		nodeCount.setPreferredSize(nodeCount.getMaximumSize());
		component.add(nodeCount);
		// Add Node Button
		JButton addNode = new JButton(addNodeImage);
		addNode.setPreferredSize(new Dimension(10, 10));
		addNode.setContentAreaFilled(false);
		addNode.setBorderPainted(false);
		addNode.addMouseListener(new MouseAdapter() {
			private boolean pressed = false;

			@Override
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e) && addNode.contains(e.getPoint())) {
					pressed = true;
					addNode.setIcon(addNodeImagePressed);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				pressed = false;
				if (SwingUtilities.isLeftMouseButton(e) && addNode.contains(e.getPoint())) {
					addNode.setIcon(addNodeImage);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				if (pressed) {
					addNode.setIcon(addNodeImagePressed);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				addNode.setIcon(addNodeImage);
			}
		});
		addNode.addActionListener(e -> {
			int nc = Integer.parseInt(nodeCount.getText());
			if (nc < 26 && graph.addNode()) {
				char node = Collections.max(graph.nodes());
				nodeCount.setText(Integer.toString(++nc));
				sourceNodeComboBox.addItem(node);
				targetNodeComboBox.addItem(node);
				updateGraph();
			}
		});
		component.add(addNode);
		// Set Highlights Button
		JButton setHighlights = new JButton("Set Highlights");
		setHighlights.setPreferredSize(new Dimension(220, 30));
		setHighlights.addActionListener(e -> {
			JCheckBox nodes[] = new JCheckBox[Integer.parseInt(nodeCount.getText()) + 1];
			nodes[0] = new JCheckBox("All", graph.highlightedNodes().size() == graph.nodes().size());
			nodes[0].addActionListener(e1 -> Arrays.stream(nodes).skip(1).forEach(n -> n.setSelected(nodes[0].isSelected())));
			final ActionListener AL = e1 -> {
				final JCheckBox thisCheckBox = (JCheckBox) e1.getSource();
				if (thisCheckBox.isSelected() && Arrays.stream(nodes).skip(1).filter(n -> n != thisCheckBox).allMatch(AbstractButton::isSelected))
					nodes[0].setSelected(true);
				else if (!thisCheckBox.isSelected() && nodes[0].isSelected())
					nodes[0].setSelected(false);
			};
			for (int i = 1; i < nodes.length; i++) {
				char n = (char) ('A' + i - 1);
				nodes[i] = new JCheckBox(Character.toString(n), graph.isHighlighted(n));
				nodes[i].addActionListener(AL);
			}

			// Dialog for setting node highlights
			JDialog dialog = new JDialog(getApp(), "Set Highlights", true) {
				@Override
				protected JRootPane createRootPane() {
					JRootPane rootPane = super.createRootPane();
					JPanel contentPane = new JPanel(new BorderLayout());
					rootPane.setContentPane(contentPane);

					JPanel centerPane = new JPanel();
					JList<JCheckBox> nodeList = new JList<>(nodes);
					nodeList.setBorder(LineBorder.createBlackLineBorder());
					nodeList.setPreferredSize(new Dimension(160, 150));
					nodeList.setFixedCellWidth(40);
					nodeList.setFixedCellHeight(20);
					nodeList.setVisibleRowCount(7);
					nodeList.setLayoutOrientation(JList.VERTICAL_WRAP);
					nodeList.setCellRenderer((list, value, index, isSelected, cellHasFocus) -> value);
					nodeList.addMouseListener(new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e) {
							int i = nodeList.locationToIndex(e.getPoint());
							if (i != -1) {
								nodes[i].doClick();
								nodeList.repaint();
							}
						}
					});
					centerPane.add(nodeList);
					contentPane.add(centerPane, BorderLayout.CENTER);

					JPanel buttonPane = new JPanel();
					JButton okButton = new JButton("OK");
					okButton.addActionListener(e1 -> dispose());
					buttonPane.add(okButton);
					contentPane.add(buttonPane, BorderLayout.SOUTH);

					return rootPane;
				}
			};
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.pack();
			dialog.setResizable(false);
			dialog.setLocationRelativeTo(getApp());
			dialog.setVisible(true);

			graph.clearHighlightedNodes();
			Arrays.stream(nodes).skip(1).filter(AbstractButton::isSelected).map(cb -> cb.getText().charAt(0)).forEach(graph::setHighlight);
			updateGraph();
		});
		component.add(setHighlights);
		sidePanel.add(component);


		// Edges
		component = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		component.setMaximumSize(new Dimension(250, 250));
		component.setPreferredSize(component.getMaximumSize());
		label = new JLabel("Edges:", SwingConstants.CENTER);
		label.setFont(headingFont);
		label.setPreferredSize(new Dimension(245, 20));
		component.add(label);
		// Edge List
		JList<Map.Entry<EndpointPair<Character>, Integer>> edgeList = new JList<>(edgeListModel = new DefaultListModel<>());
		edgeList.setBackground(Color.WHITE);
		edgeList.setForeground(Color.BLACK);
		edgeList.setSelectionBackground(new Color(0xA1C9E6));
		edgeList.setSelectionForeground(Color.BLACK);
		edgeList.setLayoutOrientation(JList.VERTICAL_WRAP);
		edgeList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		edgeList.setVisibleRowCount(6);
		edgeList.setFixedCellWidth(76);
		edgeList.setCellRenderer((list, value, index, isSelected, cellHasFocus) -> {
			JLabel ret = new JLabel(value.getKey().nodeU() + " --" + value.getValue() +
					(value.getKey().isOrdered() ? "-> " : "-- ") + value.getKey().nodeV(), SwingConstants.CENTER);
			ret.setOpaque(true);

			if (graph.isHighlighted(value.getKey())) ret.setFont(ret.getFont().deriveFont(Font.BOLD));

			if (isSelected) {
				ret.setBackground(list.getSelectionBackground());
				ret.setForeground(list.getSelectionForeground());
			} else {
				ret.setBackground(list.getBackground());
				ret.setForeground(list.getForeground());
			}

			return ret;
		});
		JScrollPane scrollList = new JScrollPane(edgeList, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollList.setBorder(LineBorder.createBlackLineBorder());
		scrollList.setPreferredSize(new Dimension(230, 100));
		edgeListModel.addListDataListener(new ListDataListener() {
			private final Dimension NO_SCROLLBAR_SIZE = new Dimension(230, 100);
			private final Dimension WITH_SCROLLBAR_SIZE = new Dimension(230, 115);

			@Override
			public void intervalAdded(ListDataEvent e) {
				if (edgeListModel.getSize() > 18 && scrollList.getPreferredSize().equals(NO_SCROLLBAR_SIZE)) {
					scrollList.setPreferredSize(WITH_SCROLLBAR_SIZE);
					scrollList.repaint();
				}
			}

			@Override
			public void intervalRemoved(ListDataEvent e) {
				if (edgeListModel.getSize() <= 18 && scrollList.getPreferredSize().equals(WITH_SCROLLBAR_SIZE)) {
					scrollList.setPreferredSize(NO_SCROLLBAR_SIZE);
					scrollList.repaint();
				}
			}

			@Override
			public void contentsChanged(ListDataEvent e) {}
		});
		component.add(scrollList);
		sourceNodeComboBox = new JComboBox<>(new DefaultComboBoxModel<>(new Character[] {'A'}));
		sourceNodeComboBox.setPrototypeDisplayValue('W');
		targetNodeComboBox = new JComboBox<>(new DefaultComboBoxModel<>());
		targetNodeComboBox.setPrototypeDisplayValue('W');
		sourceNodeComboBox.addItemListener(e -> {
			if (e.getStateChange() == ItemEvent.DESELECTED) {
				targetNodeComboBox.insertItemAt((Character) e.getItem(), ((DefaultComboBoxModel<Character>) sourceNodeComboBox.getModel()).getIndexOf(e.getItem()));
			} else {
				targetNodeComboBox.removeItem(e.getItem());
			}
		});
		edgeList.addListSelectionListener(e -> {
			if (!edgeList.isSelectionEmpty()) {
				Map.Entry<EndpointPair<Character>, Integer> edge = edgeListModel.elementAt(edgeList.getLeadSelectionIndex());
				sourceNodeComboBox.setSelectedItem(edge.getKey().nodeU());
				targetNodeComboBox.setSelectedItem(edge.getKey().nodeV());
				edgeWeightField.setText(edge.getValue().toString());
			}
		});
		component.add(sourceNodeComboBox);
		label = new JLabel("--", SwingConstants.CENTER);
		label.setPreferredSize(new Dimension(15, 10));
		component.add(label);
		component.add(edgeWeightField = new JTextField(2));
		edgeWeightField.setHorizontalAlignment(SwingConstants.CENTER);
		label = new JLabel("--", SwingConstants.CENTER);
		label.setPreferredSize(new Dimension(15, 10));
		component.add(label);
		component.add(targetNodeComboBox);

		final Dimension BUTTON_SIZE = new Dimension(110, 30);

		// Add/Edit Edge Button
		JButton addEdgeButton = new JButton("Add/Edit Edge");
		addEdgeButton.setPreferredSize(BUTTON_SIZE);
		addEdgeButton.addActionListener(e -> {
			try {
				char source = (Character) sourceNodeComboBox.getSelectedItem();
				char target = (Character) targetNodeComboBox.getSelectedItem();
				int weight = Integer.parseInt(edgeWeightField.getText());
				if (weight <= 0) throw new NumberFormatException();

				int insert_index = edgeListModel.getSize();
				Map.Entry<EndpointPair<Character>, Integer> edge = null;
				{
					EndpointPair<Character> newEdge = graph.isDirected() ? EndpointPair.ordered(source, target) : EndpointPair.unordered(source, target);
					for (int i = 0; i < edgeListModel.getSize(); i++) {
						EndpointPair<Character> currentEdge = edgeListModel.get(i).getKey();
						if (currentEdge.equals(newEdge)) {
							edge = edgeListModel.get(i);
							break;
						} else if (insert_index == edgeListModel.getSize() && ((currentEdge.nodeU() == source &&
								currentEdge.nodeV() > target) || currentEdge.nodeU() > source)) {
							insert_index = i;
						}
					}
				}

				if (edge == null) {
					edgeListModel.add(insert_index, new AbstractMap.SimpleEntry<>(directedButton.isSelected() ?
							EndpointPair.ordered(source, target) : EndpointPair.unordered(target, source), weight));
				} else if (edge.getValue() != weight) {
					edgeList.setSelectedValue(edge, true);
					if (JOptionPane.showConfirmDialog(getApp(), "An edge from " + source + " to " + target +
									" already exists with a weight of " + edge.getValue() + ". Would you like to replace the weight with " + weight + "?",
							"Replace Edge?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							edge.setValue(weight);
							edgeList.repaint();
					} else return;
				} else return;

				graph.addEdge(source, target, weight);
				updateGraph();
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(getApp(), "Edge weight must be a positive integer!", "Error", JOptionPane.ERROR_MESSAGE);
				edgeWeightField.requestFocusInWindow();
				edgeWeightField.selectAll();
			} catch (NullPointerException e1) {
				JOptionPane.showMessageDialog(getApp(), "Both a source and target node must be specified!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		component.add(addEdgeButton);

		// Remove Edge Button
		JButton removeEdgeButton = new JButton("Remove Edge");
		removeEdgeButton.setPreferredSize(BUTTON_SIZE);
		removeEdgeButton.setEnabled(false);
		removeEdgeButton.addActionListener(e -> {
			edgeList.getSelectedValuesList().forEach(edge -> graph.removeEdge(edge.getKey().nodeU(), edge.getKey().nodeV()));
			edgeList.getSelectedValuesList().forEach(edgeListModel::removeElement);
			updateGraph();
		});
		component.add(removeEdgeButton);

		// Set Highlight Button
		JButton setHighlightButton = new JButton("Set Highlight");
		setHighlightButton.setPreferredSize(BUTTON_SIZE);
		setHighlightButton.setEnabled(false);
		setHighlightButton.addActionListener(e -> {
			edgeList.getSelectedValuesList().forEach(edge -> graph.setHighlight(edge.getKey()));
			edgeList.repaint();
			updateGraph();
		});
		component.add(setHighlightButton);

		// Clear Highlight Button
		JButton clearHighlightButton = new JButton("Clear Highlight");
		clearHighlightButton.setPreferredSize(BUTTON_SIZE);
		clearHighlightButton.setEnabled(false);
		clearHighlightButton.addActionListener(e -> {
			edgeList.getSelectedValuesList().forEach(edge -> graph.clearHighlight(edge.getKey()));
			edgeList.repaint();
			updateGraph();
		});
		component.add(clearHighlightButton);

		edgeList.addListSelectionListener(e -> {
			removeEdgeButton.setEnabled(!edgeList.isSelectionEmpty());
			setHighlightButton.setEnabled(!edgeList.isSelectionEmpty());
			clearHighlightButton.setEnabled(!edgeList.isSelectionEmpty());
		});
		sidePanel.add(component);

		add(sidePanel, BorderLayout.EAST);


		// Graph
		JPanel graphPanel = new JPanel();
		graphPanel.setLayout(new BoxLayout(graphPanel, BoxLayout.Y_AXIS));
		graphPanel.add(Box.createVerticalGlue());
		graphPanel.add(graphImage = new JLabel());
		graphImage.setAlignmentX(CENTER_ALIGNMENT);
		graphPanel.add(Box.createVerticalGlue());
		add(graphPanel, BorderLayout.CENTER);

		buildGraph();


		// Buttons
		JPanel buttonPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(0, 40));
		buttonPanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));

		// Set up "Back" button
		JButton backButton = new JButton("Back");
		backButton.addActionListener(e -> getApp().mainMenu());
		buttonPanel.add(backButton);

		// Set up "Options" button
		JButton optionsButton = new JButton("Options");
		optionsButton.addActionListener(e -> getApp().optionsMenu(this));
		buttonPanel.add(optionsButton);

		// Set up "Load" button
		JButton loadButton = new JButton("Load Graph");
		loadButton.addActionListener(new LoadHandler());
		buttonPanel.add(loadButton);

		// Set up "Save Graph" button
		JButton saveButton = new JButton("Save Graph");
		saveButton.addActionListener(new SaveHandler(graph, ((BufferedImage) ((ImageIcon) graphImage.getIcon()).getImage())));
		buttonPanel.add(saveButton);

		// Set up "Use Graph" button
		JButton useButton = new JButton("Use Graph");
		useButton.addActionListener(e -> getApp().graphMenu(graph, this));
		buttonPanel.add(useButton);

		add(buttonPanel, BorderLayout.SOUTH);

		setMinimumSize(new Dimension(625, 500));
	}

	/**
	 * Loads a graph to be manipulated
	 * @param graph the graph to be loaded
	 */
	void loadGraph(Graph graph) {
		this.graph = graph;
		JRadioButton db = graph.isDirected() ? directedButton : undirectedButton;
		ItemListener dbil = db.getItemListeners()[0];
		db.removeItemListener(dbil);
		db.setSelected(true);
		db.addItemListener(dbil);
		nodeCount.setText(Integer.toString(graph.nodes().size()));
		edgeListModel.removeAllElements();
		graph.edges().entrySet().stream().sorted(Comparator.comparing(e -> ((Map.Entry<EndpointPair<Character>, Integer>) e).getKey().nodeU())
		.thenComparing(e -> ((Map.Entry<EndpointPair<Character>, Integer>) e).getKey().nodeV())).forEach(edgeListModel::addElement);
		ItemListener il = sourceNodeComboBox.getItemListeners()[0];
		sourceNodeComboBox.removeItemListener(il);
		sourceNodeComboBox.removeAllItems();
		graph.nodes().stream().sorted().forEach(sourceNodeComboBox::addItem);
		graph.nodes().stream().sorted().skip(1).forEach(targetNodeComboBox::addItem);
		sourceNodeComboBox.addItemListener(il);

		updateGraph();
	}

	/** Builds a graph using the specified parameters */
	private void buildGraph() {
		Graph oldGraph = graph;
		graph = directedButton.isSelected() ? new DirectedGraph() : new UndirectedGraph();
		graph.addNodes(Integer.parseInt(nodeCount.getText()));
		edgeListModel.elements().asIterator().forEachRemaining(edge -> graph.addEdge(edge.getKey().nodeU(), edge.getKey().nodeV(), edge.getValue()));
		if (oldGraph != null) {
			oldGraph.highlightedNodes().forEach(graph::setHighlight);
			oldGraph.highlightedEdges().forEach(edge -> graph.setHighlight(edge.nodeU(), edge.nodeV()));
		}

		updateGraph();
	}

	/** Updates the graph image */
	void updateGraph() {
		graphImage.setIcon(new ImageIcon(Common.dot(graph, false, true)));
		graphImage.repaint();
		setMinimumSize(new Dimension(graphImage.getIcon().getIconWidth() + 270, graphImage.getIcon().getIconHeight() + 80));
	}
}
