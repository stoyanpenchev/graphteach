package GUI;

import GUI.utility.JPanel;

import javax.swing.*;
import java.awt.*;

import static GUI.GraphTeach.getApp;

/** Main menu singleton */
class MainMenu extends Menu {

	/** The active main menu */
	private static MainMenu MAIN_MENU;

	/**
	 * Initializes and returns the active main menu
	 * @return the active main menu
	 */
	static MainMenu getMainMenu() {
		if (MAIN_MENU == null) MAIN_MENU = new MainMenu();
		return MAIN_MENU;
	}

	/** Creates the main menu */
	private MainMenu() {
		super(new BorderLayout(), null);

		// Sets up the welcome message
		JTextArea welcomeMessage = new JTextArea("Welcome to\nGraphTeach");
		welcomeMessage.setEditable(false);
		welcomeMessage.setFont(new Font("Calibri", Font.ITALIC, 60));
		welcomeMessage.setBorder(null);
		JPanel centerPane = new JPanel(new GridBagLayout());
		centerPane.add(welcomeMessage, new GridBagConstraints(0, 0, 1 ,1, 0, 0, GridBagConstraints.CENTER, 0, new Insets(0, 0, 0, 0), 0, 0));
		add(centerPane, BorderLayout.CENTER);

		JPanel buttonPane = new JPanel(new FlowLayout());
		add(buttonPane, BorderLayout.SOUTH);

		// Set up "Create Graph" button
		JButton createGraphButton = new JButton("Create Graph");
		createGraphButton.addActionListener(e -> getApp().graphCreation());
		buttonPane.add(createGraphButton);

		// Set up "Load Graph" button
		JButton loadGraphButton = new JButton("Load Graph");
		loadGraphButton.addActionListener(new LoadHandler());
		buttonPane.add(loadGraphButton);

		// Set up "Options" button
		JButton optionsButton = new JButton("Options");
		optionsButton.addActionListener(e -> getApp().optionsMenu(this));
		buttonPane.add(optionsButton);

		// Set up the "About" button
		JButton aboutButton = new JButton("About");
		aboutButton.addActionListener(e -> JOptionPane.showMessageDialog(getApp(), "GraphTeach is a tool designed to aid in the teaching of basic\n" +
				"graph algorithms by providing helpful visualization and\ndetailed description of each step.\n\n" +
				"Created by Stoyan Penchev\nEmail: spench@essex.ac.uk", "About GraphTeach", JOptionPane.PLAIN_MESSAGE));
		buttonPane.add(aboutButton);

		// Set up "Exit" button
		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(e -> getApp().dispose());
		buttonPane.add(exitButton);

		setMinimumSize(new Dimension(375, 250));
		setPreferredSize(new Dimension(500, 500));
	}
}
