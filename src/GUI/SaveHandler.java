package GUI;

import backend.Common;
import backend.graphs.GraphView;
import backend.graphs.MCST;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static GUI.GraphTeach.getApp;

/** Class for handling saving of graph to different formats */
class SaveHandler implements ActionListener {

	private static JFileChooser fileChooser = new JFileChooser();
	private static FileNameExtensionFilter graphFileFilter = new FileNameExtensionFilter("GRAPH File", "graph");
	private static FileNameExtensionFilter graphTeachFileFilter = new FileNameExtensionFilter("GraphTeach File", "gt");
	static {
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG Image", "png"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("HTML Document", "html"));
	}

	/**
	 * Enables/disables graph-related file formats depending on whether an actual graph object was supplied
	 * @param graphProvided the value of {@code graph != null}
	 */
	private static void setupFileChooser(boolean graphProvided) {
		if (graphProvided) {
			fileChooser.addChoosableFileFilter(graphFileFilter);
			fileChooser.addChoosableFileFilter(graphTeachFileFilter);
		} else {
			fileChooser.removeChoosableFileFilter(graphFileFilter);
			fileChooser.removeChoosableFileFilter(graphTeachFileFilter);
		}
	}

	/** The graph to be saved. Can be {@code null}. */
	private GraphView graph;
	/** The image to be saved */
	private BufferedImage image;

	/**
	 * Creates a save handler for the specified graph
	 * @param graph the graph to be saved
	 */
	SaveHandler(GraphView graph) {
		this(Objects.requireNonNull(graph), null);
	}

	/**
	 * Creates a save handler for the specified image
	 * @param img the image to be saved
	 */
	SaveHandler(BufferedImage img) {
		this(null, Objects.requireNonNull(img));
	}

	/**
	 * Creates a save handler
	 * @param graph the graph to be saved  or {@code null}
	 * @param img the image to be saved
	 */
	SaveHandler(GraphView graph, BufferedImage img) {
		this.graph = graph;
		if (graph != null && img == null) image = Common.dot(graph instanceof MCST ? (MCST) graph : graph);
		else image = Objects.requireNonNull(img);
	}

	@SuppressWarnings("SpellCheckingInspection")
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			setupFileChooser(graph != null);
			if (fileChooser.showSaveDialog(getApp()) == JFileChooser.APPROVE_OPTION) {
				File selected = fileChooser.getSelectedFile();
				if (!selected.getName().contains("."))
					selected = new File(selected.getAbsolutePath() + "." + ((FileNameExtensionFilter) fileChooser.getFileFilter()).getExtensions()[0]);
				if (selected.getName().endsWith(".graph")) { // GRAPH File
					try (PrintWriter fout = new PrintWriter(selected)) {
						fout.println("directed: " + graph.isDirected());
						List<Character> nodes = graph.nodes().stream().sorted().collect(Collectors.toList());
						if (nodes.get(nodes.size() - 1) - nodes.get(0) + 1 == nodes.size())
							fout.println("nodes: " + nodes.size());
						else fout.println("nodes: " + nodes.stream().map(Object::toString).collect(Collectors.joining(", ")));
						graph.edges().forEach((edge, weight) -> fout.println(edge.nodeU() + " -" + weight + (graph.isDirected() ? "-> " : "- ") + edge.nodeV()));
						if (graph.highlightedNodes().size() != 0)
							fout.println("highlighted-nodes: " + graph.highlightedNodes().stream().sorted().map(Object::toString).collect(Collectors.joining(",")));
						if (graph.highlightedEdges().size() != 0)
							fout.println("highlighted-edges: " + graph.highlightedEdges().stream().map(edge -> edge.nodeU() + (graph.isDirected() ? "->" : "-") + edge.nodeV()).collect(Collectors.joining(";")));
					}
				} else if ( selected.getName().endsWith(".gt")) { // GraphTeach File
					try (ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream(selected))) {
						fout.writeObject(graph);
					}
				} else if (selected.getName().endsWith(".png")) { // PNG File
					ImageIO.write(image, "png", selected);
				} else if (selected.getName().endsWith(".html")) { // HTML File
					ByteArrayOutputStream imageStream = new ByteArrayOutputStream();
					ImageIO.write(image, "png", imageStream);

					try (PrintWriter fout = new PrintWriter(selected)) {
						fout.println("<!DOCTYPE HTML>");
						fout.println("<html>");
						fout.println("\t<head>");
						fout.println("\t\t<meta charset=\"UTF-8\">");
						fout.println("\t\t<title>Graph</title>");
						if (graph != null) { // If graph object is supplied add metadata to HTML
							fout.println("\t\t<meta name=\"generator\" content=\"GraphTeach\"");
							fout.println("\t\t\tdata-directed=\"" + graph.isDirected() + "\"");
							fout.print("\t\t\tdata-nodes=\"");
							List<Character> nodes = graph.nodes().stream().sorted().collect(Collectors.toList());
							if (nodes.get(nodes.size() - 1) - nodes.get(0) + 1 == nodes.size()) fout.println(nodes.size() + "\"");
							else fout.println(nodes.stream().map(Object::toString).collect(Collectors.joining(",")) + "\"");
							fout.print("\t\t\tdata-edges=\"");
							fout.print(graph.edges().entrySet().stream().map(entry -> entry.getKey().nodeU() + "-" + entry.getValue() +
									(graph.isDirected() ? "->" : "-") + entry.getKey().nodeV()).collect(Collectors.joining(";")));
							fout.println("\"");
							if (graph.highlightedNodes().size() != 0)
								fout.println("\t\t\tdata-highlighted-nodes=\"" + graph.highlightedNodes().stream().sorted().map(Object::toString).collect(Collectors.joining(",")) + "\"");
							if (graph.highlightedEdges().size() != 0)
								fout.println("\t\t\tdata-highlighted-edges=\"" + graph.highlightedEdges().stream().map(edge -> edge.nodeU() +
										(graph.isDirected() ? "->" : "-") + edge.nodeV()).collect(Collectors.joining(";")) + "\"");
							fout.println("\t\t>");
						}
						fout.println("\t</head>");
						fout.println("\t<body>");
						fout.print("\t\t<img style=\"display: block; margin: auto;\" src=\"data:image/png;base64,");
						StringBuilder str = new StringBuilder(Base64.getEncoder().encodeToString(imageStream.toByteArray()));
						String eol = System.lineSeparator();
						for (int i = 0; i < str.length(); i += 103 + eol.length()) str.insert(i, eol + "\t\t\t");
						fout.print(str.toString());
						fout.println("\" alt=\"graph.png\">");
						fout.println("\t</body>");
						fout.println("</html>");
					}
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(getApp(), ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
